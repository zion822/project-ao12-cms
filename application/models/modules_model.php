<?php
class Modules_model extends CI_model{
    public function get_modules(){
        $query = $this->db->get('modules');
        
        if($query->num_rows > 0){
            $data = $query->result();
            return $data;
        }
        else{
            return false;
        }
    }
    
    public function get_module($mid){
        $this->db->where('mid', $mid);
        $query = $this->db->get('modules');
        
        if($query->num_rows > 0){
            $data = $query->result();
            return $data;
        }
        else{
            return false;
        }
    }
    
    public function activate($mid){
        $data = array(
            'mod_active' => 1
        );
        $this->db->where('mid', $mid);
        $query = $this->db->update('modules', $data);
            
        if($query){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function deactivate($mid){
        $data = array(
            'mod_active' => 0
        );
        
        $this->db->where('mid', $mid);
        $query = $this->db->update('modules', $data);
        
        if($query){
            return true;
        }
        else{
            return false;
        }
    }
}
?>