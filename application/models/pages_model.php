<?php
class Pages_model extends ci_model{
    public function get_pages(){
        $this->db->select('p_id, page_title');
        $this->db->order_by('order', 'asc');
        $query = $this->db->get('pages');
        
        if($query->num_rows() > 0){
            $data = $query->result();
            return $data;
        }
        else{
            return false;  
        }
    }
    public function get_page($getPage){
        $query = $this->db->get_where('pages', array('page_title' => $getPage));
        
        if ($query->num_rows() > 0){
				$data = $query->result();
				return $data;
			}
			else{
				return false;
			}
        
    }
    
    public function first_page(){
        $this->db->limit('1');
        $this->db->where('order', 1);
       $query = $this->db->get('pages');
        
        if($query){
            $data = $query->result();
            return $data;
        }
        else{
            return false;
        }
    }
    
    public function new_page(){
        $title = str_replace(' ', '-', $this->input->post('title'));
        
        $query = $this->db->get('pages');
        
        if($query->num_rows() > 0){
            $data = array (
            'page_title' => $title,
            'content' => $this->input->post('content')
            );
        }
        else{
            $data = array (
            'page_title' => $title,
            'content' => $this->input->post('content'),
            'order' => 1
            );
        }        
        
        $query = $this->db->insert('pages', $data);
        
        if ($query){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function save_page_order(){
        $ids = $this->input->post('s', TRUE);  //this is just "s", because that is what my id's started with, "s_{id}". Sortable() does this for you
        
         foreach($ids as $order => $item_id){
            $this->db->where('p_id', $item_id);
            $this->db->update('pages', array('order' => $order + 1));
         }
        return true;
    }
    
    public function retrieve_page(){
        $id = $this->input->get('id');
        $query = $this->db->get_where('pages', array('p_id' => $id));

        if ($query->num_rows() > 0){
            $data = $query->result();
            return $data;
        }
        else{
        return false;
        }
	}
    
    public function update_page(){
       $id = $this->input->post('id');
        $data = array (
            'page_title' => $this->input->post('title'),
            'content' => $this->input->post('content'),
            );

        $this->db->where('p_id', $id);

        $query = $this->db->update('pages', $data);

        if ($query){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function delete_page(){
       $pid = $this->input->get('pid');
		
		$query = $this->db->delete('pages', array('p_id' => $pid)); 
		
		if($this->db->affected_rows() == 1){
			return true;
		}
		else{
			return false;
		}
    }
    
} // End class

?>