<?php
	class Users_model extends ci_model{

		public function can_log_in(){
			$password = $this->input->post('password');
			$pass = $this->encrypt->sha1($password);

			$this->db->where('email', $this->input->post('username'));

			$this->db->where('password', $pass);
			$this->db->select('u_id, role, firstname, lastname');
		

			$query = $this->db->get('users');
 
			if($query->num_rows() == 1){
				
				foreach ($query->result() as $value) {
					$u_id = $value->u_id;
					$role = $value->role;
                    $firstname = $value->firstname;
                    $lastname = $value->lastname;

					$data = array (
						'u_id' => $u_id,
						'role' => $role,
                        'firstname' => $firstname,
                        'lastname' => $lastname
					);
				}
				$this->session->set_userdata($data);
				return true;
			}else{
				return false;
			}
		}
		//ophalen van alle users voor het userbeheer systeem
		public function getAllUsers(){
			$query = $this->db->get('users');
			if($query->num_rows() >= 1){
				$data = $query->result();	
				return $data;
			}
		}
        
        public function insertUser(){
            //CREATING RANDOM PASSWORD FOR FIRST LOGIN
            $a = "abcdBCZ";
            $b = "aBeFt9c";
            $pwd = substr(str_shuffle($a),0, 8) . substr(str_shuffle($b),0, 31);
            //$pwdH
            $pwdH = sha1($this->input->post('pwd'));

            //CREATING HASH FOR MAIL INVITATION TO ACTIVATE ACCOUNT
            $hash = sha1(rand().microtime());

            //FETCHING NEW USER DATA INTO ARRAY
            $data = array (
            'firstname'    => ucfirst($this->input->post('firstname')),
            'lastname'     => ucfirst($this->input->post('lastname')),
            'email'        => $this->input->post('email'),
            'role'  	   => $this->input->post('userlvl'),
            'password'     => $pwdH,
            'email_code'   => $hash
            );

            //INSERTING NEW USER IN DB
            $this->db->insert('users', $data);

           
            $to = $this->input->post('email');
            $subject = "Account bevestiging van Simplicity";
            $message = "<html>
                            <head>
                                <title>Account bevestiging van Simplicity</title>
                            </head>
                            <body>
                            <h1>Simplicity</h1>
                                Geachte heer/mevrouw ".ucfirst($this->input->post('firstname'))." ".ucfirst($this->input->post('lastname'))."<br>
                                Er is een account voor u aangemaakt binnen het CMS Simplicity.<br>
                                Door op de onderstaande link te klikken activeert uw het account en wordt u verzogt een wachtwoord in te stellen.<br>
                                <br>
                                <a href=".base_url('/admin/activation?c=').$data['email_code'].">Activate account.</a>
                                <br><br>
                                Website: <a href=".base_url().">www.simplicity.nl</a><br>
                                Email: <a href='mailto:info@simplicity.nl'>info@simplicity.nl</a>

                            </body>
                        </html>";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers.="Content-type: text/html; charset=\"UTF-8\" \r\n";
            $headers .= 'From: <noreply@simplicity.nl>' . "\r\n";
            mail($to,$subject,$message,$headers);
            //REDIRECT TO USERS PAGE
            redirect('users');
        }
        public function removeuser($uid){
            $current_user = $this->session->userdata('u_id');
            $this->db->where('u_id', $uid);
            $this->db->delete('users');
        }

        public function getUserByiD($uid){
        $this->db->select('*');
        $this->db->where('u_id', $uid);
        $query = $this->db->get('users');

            foreach ($query->result() as $value){
                $name       = $value->firstname;
                $lastname   = $value->lastname;
                $email      = $value->email;
                $role       = $value->role;
                $uid        = $value->u_id;
                $language   = $value->language;
                $active     = $value->active;

                $data = array (
                    'firstname' => $name,
                    'lastname'  => $lastname,
                    'email'     => $email,
                    'role'      => $role,
                    'u_id'      => $uid,
                    'language'  => $language,
                    'active'    => $active
                );
            }
            return $data;
        }
        public function update(){
            //CREATING DATA ARRAY FOR USER UPDATE
            $data = array (
                          'firstname'   => ucfirst($this->input->post('firstname')),
                          'lastname'    => ucfirst($this->input->post('lastname')),
                          'email'       => $this->input->post('email'),
                          'role'        => $this->input->post('userlvl')
                          );

            //UPDATING DATA IN DB
            $this->db->update('users', $data, "u_id = '".$this->input->post('uid')."'");
        }
        public function update_mysettings(){
              //CREATING DATA ARRAY FOR USER UPDATE
            $data = array (
                          'firstname'   => ucfirst($this->input->post('firstname')),
                          'lastname'    => ucfirst($this->input->post('lastname')),
                          'email'       => $this->input->post('email'),
                          'language'    => $this->input->post('language')
                          );

            //UPDATING DATA IN DB
            $this->db->update('users', $data, "u_id = '".$this->session->userdata('u_id')."'");
        }
        public function emailExists(){
            $email = $this->input->post('email');

            $this->db->where('email', $this->input->post('email'));
            $query = $this->db->get('users');
 
            if($query->num_rows() == 1){
                return true;
            }else{
                return false;
            }
        }
        
        public function sentRecoverMail(){


            $hash = sha1(rand().microtime());
            //FETCHING NEW USER DATA INTO ARRAY
            $data = array (
            'recover_code'   => $hash
            );

            //INSERTING NEW USER IN DB

            
        $this->db->where('email', $this->input->post('email'));
        $this->db->update('users', $data);


            $this->db->select('*');
            $this->db->where('email', $this->input->post('email'));
            $query = $this->db->get('users');

            foreach ($query->result() as $value){
                $name       = $value->firstname;
                $lastname   = $value->lastname;
                $email      = $value->email;
                $recovercode = $value->recover_code;

                
                $data = array (
                    'name'          => $name,
                    'lastname'      => $lastname,
                    'email'         => $email,
                    'recover_code'  => $recovercode
                );
            }

            $to = $this->input->post('email');
            $subject = "Simplicity account recovery";
            $message = "<html>
                            <head>
                                <title>Account bevestiging van Simplicity</title>
                            </head>
                            <body>
                            <h1>Simplicity</h1>
                                Geachte heer/mevrouw ".ucfirst($data['name'])." ".ucfirst($data['lastname'])."<br>
                                Klik op de onderstaande link om uw wachtwoord te resetten.
                                <br>
                                <a href='".base_url('/admin/startRecover?c=').$hash."'>Reset uw wachtwoord</a>
                                <br><br>
                                Website: <a href=".base_url().">www.simplicity.nl</a><br>
                                Email: <a href='mailto:info@simplicity.nl'>info@simplicity.nl</a>
                            </body>
                        </html>";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers.="Content-type: text/html; charset=\"UTF-8\" \r\n";
            $headers .= 'From: <noreply@simplicity.nl>' . "\r\n";
            mail($to,$subject,$message,$headers);
            redirect("admin/login");
            }
        public function isRecoverCodeTrue(){
            $code = $_GET['c'];

            $this->db->where('recover_code', $code);
            $query = $this->db->get('users');
 
            if($query->num_rows() == 1){
                return true;
            }else{
                return false;
            }
        }

        public function setNewPassword(){

            $this->db->select('u_id');
            $this->db->where('recover_code', $this->session->userdata('code'));
            $query = $this->db->get('users');

             foreach ($query->result() as $value){
                $id       = $value->u_id;
            }

            $data = array (
            'password'   => sha1($this->input->post('password'))
            );


            $this->db->where('u_id', $id);
            $this->db->update('users', $data);

            $this->session->sess_destroy();
        }
        public function activeAccount(){

            $this->db->select('u_id');
            $this->db->where('email_code', $this->session->userdata('emailcode'));
            $query = $this->db->get('users');

            foreach ($query->result() as $value){
                $id = $value->u_id;
            }

            $data = array (
            'password'   => sha1($this->input->post('password')),
            'active' => 1
            );

            $this->db->where('u_id', $id);
            $this->db->update('users', $data);

            $this->session->sess_destroy();
        }
        public function isEmailTrue(){
             $code = $_GET['c'];

            $this->db->where('email_code', $code);
            $query = $this->db->get('users');
 
            if($query->num_rows() == 1){
                return true;
            }else{
                return false;
            }
        }
        public function user_active(){

            $this->db->where('email', $this->input->post('username'));

            $this->db->select('active');
        
            $query = $this->db->get('users');
 
            if($query->num_rows() == 1){
                
                foreach ($query->result() as $value) {
                    $active = $value->active;
                   
                    if($active == 1){
                        return true;
                    }else{
                        return false;
                   }
                }
            }
        }        
	}
?>