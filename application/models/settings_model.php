<?php
class Settings_model extends ci_model{
    
    // Get settings
    public function get_settings(){
        $query = $this->db->get('settings');
        
        if($query){
            $data = $query->result();
            return $data;
        }
        else{
        }
    }
    public function update_settings(){
        $data = array(
                array(
                    'setting_id' => 1,
                    'setting_value' => $this->input->post('site_title'),
                ),
                array(
                    'setting_id' => 2,
                    'setting_value' => $this->input->post('site_slogan')
                ),
                array(
                    'setting_id' => 3,
                    'setting_value' => $this->input->post('admin_email')
                )
        );
        $query = $this->db->update_batch('settings', $data, 'setting_id');
        if($this->db->trans_status() === TRUE){
            return true;
        }
        else{
            return true;
        }
    }
    
    public function get_language(){
        if($this->session->userdata('u_id')){
            
            $this->db->select('language');
            $this->db->where('u_id', $this->session->userdata('u_id'));
            $query = $this->db->get('users');
            
            $row = $query->row();
            
            return $row->language;
        }else{
           return "english";//DEFAULT
        }
    }
 
    public function get_site_title(){
        $this->db->select('setting_value');
        $this->db->where('setting_name', 'site_title');
        $query = $this->db->get('settings');
        
        $row = $query->row();
        
        return $row->setting_value;
    }
}
?>