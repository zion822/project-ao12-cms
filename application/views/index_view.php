<?php
foreach ($page as $row){
    $title = $row->page_title;
    $content = $row->content;
}
?>
<html>
    <head>
        <?php echo link_tag("assets/frontend.css"); ?>
        <title><?php echo $title . ' - ' . $site_title;?></title>
    </head>
    <body>
        <header>
            
            <div class="container">
                <div class="grid_3">
                    <?php echo '<img class="logo_header" src="' . base_url('assets/images/logo2.png') .'" />' ?>
                </div>
            
            </div><!--.container -->
            
        </header>
        
        <nav>
                

                    <ul class="nav">
                    <?php
                    foreach($pages as $row){
                        $ptitle = strtolower($row->page_title);
                        echo '<li><a href="'.base_url().''.$ptitle.'">'.$row->page_title.'</a></li>';
                    }?>
                    </ul>

                
        </nav>
        <div class="container">
                <div class="content">
                    <?php 
                        echo '<h1>'.$title.'</h1>'; 
                        echo $content;
                    ?>
                </div>
        </div>
        <footer>
                <p>&copy;2014 Simplicity. All rights reserved.</p>
        </footer>
    </body>
</html>