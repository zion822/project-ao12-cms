<?php
$html = '<!DOCTYPE html> 
	<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="'. base_url('assets/favicon.png') .'"/>

	<title>Recover - Simplicity</title>

	<!-- Bootstrap core CSS -->
	<link href="' . base_url('assets/css/bootstrap.css') .'" rel="stylesheet" type="text/css" />
	<link href="' . base_url('assets/css/login.css') .'" rel="stylesheet" type="text/css" />

     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="login-container">';
  
	$attributes = array('class' => 'form-signin', 'name' => 'login', 'autocomplete' => 'off');
	$email = array('name' => 'email', 'required' => 'required', 'autofocus' => 'autofocus', 'class' => 'form-control', 'placeholder' => 'Email address');
	$submit = array('name' => 'recover_submit', 'value' => 'Recover!', 'class' => 'submit-button');

	$html .= form_open('admin/recoveryValidate', $attributes);

	$html .= "<h2 class='form-signin-heading'>Password recovery</h2>";
			if($this->session->flashdata('errors')){
    		$html .= '  <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="fa fa-times"></i> <strong>'.lang('error').'</strong> ' . $this->session->flashdata('errors') . '</div>';
    };
	$html .= "<p>Please enter your e-mail address.<br>You will receive an e-mail within 5 minutes.</p>";
	$html .= validation_errors();

	$html .= form_input($email);
	$html .= "<br>".form_submit($submit);
	$html .= form_close();
	

	$html .= '
	
	</div> <!-- /container -->
    
    <div class="logo">
    <img class="login-logo" src="' . base_url('assets/images/logo.png') .'" />
    </div>
    

  </body>
</html>';
echo $html;
?>