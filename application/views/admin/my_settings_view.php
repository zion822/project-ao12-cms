<div id="content">
	<div id="innerContent">
		<h1>My settings</h1>
		<div id="usersManagement">
			    <?php
                if($this->session->flashdata('success')){
                    echo '  <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <i class="fa fa-check"></i> <strong>Success!</strong> ' . $this->session->flashdata('success') . '
                            </div>'; 
                }
                if ($this->session->flashdata('errors')){
                    echo '  <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <i class="fa fa-times"></i> <strong>The following errors occured:</strong> ' . $this->session->flashdata('errors') . ' 
                            </div>';
                }

                $attributes = array('class' => 'form-pdf', 'name' => 'login', 'autocomplete' => 'off');
                $name       = array('name' => 'firstname', 'required' => 'required', 'class' => 'form-control', 'placeholder' => '','value' => $userDetails['firstname']);
                $lastname   = array('name' => 'lastname', 'required' => 'required', 'class' => 'form-control', 'placeholder' => '', 'value' => $userDetails['lastname']);
                $email      = array('name' => 'email', 'required' => 'required', 'class' => 'form-control', 'placeholder' => '', 'value' => $userDetails['email']);
                $options    = array('dutch' => lang('settings_language_nl'), 'english' => lang('settings_language_en'));
                $hidden     = array('omail' => $userDetails['email']);
               
                $submit     = array('name' => 'login_submit', 'value' => 'Save settings', 'class' => 'btn btn-success');
                
                echo form_open('users/settings_validation', $attributes, $hidden);
                echo validation_errors(); ?>
                <label class="control-label" for="Name">Name</label><?php echo form_input($name); ?>
                <label class="control-label" for="Last name">Lastname</label><?php echo form_input($lastname); ?>
                <label class="control-label" for="Email">Email</label><?php echo form_input($email); ?>
                    <div class='control-group'>
            <label class='control-label' for='language'><?php echo lang('settings_language');?></label>
            <div class='controls'>
                <?php echo form_dropdown('language', $options, ''.$userDetails['language'].'', 'class="input-xlarge" id="language"' );?>
            </div>
        </div>
        <?php
                echo '<br>';
                echo form_submit($submit);
                echo form_close();
            ?>
		</div>
	</div>
</div>
