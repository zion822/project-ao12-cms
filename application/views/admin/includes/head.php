<?php
$currentPage = $this->uri->segment(2);
if(!$currentPage){
    $currentPage = $this->uri->segment(1);
}
else{
}
if($currentPage == "dashboard" OR $currentPage == "admin"){$dashactive = "active-page";}else{$dashactive = "";}     // dashboard
if($currentPage == "pages" || $currentPage == "new-page"){$pageactive = "active-page";}else{$pageactive = "";}      // pages
if($currentPage == "users" || $currentPage == "permissions"){$useractive = "active-page";}else{$useractive = "";}   // users
if($currentPage == "settings"){$systemactive = "active-page";}else{$systemactive = "";}   // modules
if($currentPage == "mysettings"){$mysettingsactive = "active-page";}else{$mysettingsactive = "";}   // mysettings
if($currentPage == "modules"){$modulesactive = "active-page";}else{$modulesactive = "";}   // Modules
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $page_title; ?></title>
		<?php echo link_tag("assets/css/bootstrap.css"); ?>
		<?php echo link_tag("assets/css/style.css"); ?>
        <?php echo link_tag("assets/js/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.css"); ?>
        <?php echo link_tag("assets/css/font-awesome-4.0.3/css/font-awesome.css"); ?>
        <link rel="shortcut icon" href="<?php echo base_url('assets/favicon.png'); ?>"/>

        <script src="<?php echo base_url('assets/js/jquery-1.11.0.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.js'); ?>" type="text/javascript"></script>
	</head>
	<body>
	<div id="wrapper">
		<?php
			$firstname = $this->session->userdata('firstname');
			$lastname = $this->session->userdata('lastname');
		?>
		<div id ="topBar">
			<div id="returnweb">
				<a href="<?php echo base_url(); ?>"target="_blank"><?php echo "Ga naar website"; ?></a>
			</div>
			 <div id="welcomeUser">
	            <h3><?php echo lang('welcome'); ?><a href="<?php echo base_url('users/mysettings'); ?>"><?php echo " ".$firstname." ".$lastname;?></a></h3>
	        </div>
    	</div>
		<div id="navPosition">
			<nav>
				<div id="cssmenu">
					<?php
					if ($this->session->userdata('is_logged_in') AND $this->session->userdata('role') == 1){
					?>

					<ul>
					   <li><a href="<?php echo base_url('admin/dashboard');?>"><span class="<?php echo $dashactive;?>"><i class="fa fa-tachometer"></i> Dashboard</span></a></li>
					   <li><a href="<?php echo base_url('admin/pages'); ?>"><span class="<?php echo $pageactive;?>"><i class="fa fa-files-o"></i> <?php echo lang('pages');?></span></a>
					   </li>
					   <li><a href="<?php echo base_url('admin/users');?>"><span class="<?php echo $useractive;?>"><i class="fa fa-users"></i> <?php echo lang('manage_users');?></span></a></li>
					   <li><a href="<?php echo base_url('users/mysettings');?>"><span class="<?php echo $mysettingsactive;?>"><i class="fa fa-user"></i> <?php echo lang('mysettings');?></span></a>
					   <li><a href="<?php echo base_url('admin/#'); ?>"><span class="<?php echo $modulesactive;?>"><i class="fa fa-puzzle-piece"></i> Modules</span></a>
                            <ul>
                                <li><a href="<?php echo base_url('admin/modules'); ?>"><span> <?php echo lang('manage_modules');?></span></a></li>
                                <?php foreach($modules as $module){
                                    if($module->mod_active == 1){
                                        echo '<li><a href="'.base_url("admin/modules/edit?mod=").$module->mid.'"><span>'.$module->mod_name.'</span></a></li>';   
                                    }
                                    else{}
                                }?>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url('admin/#'); ?>"><span class="<?php echo $systemactive;?>"><i class="fa fa-wrench"></i> <?php echo lang('system');?></span></a>
                            <ul>
                                <li><a href="<?php echo base_url('admin/settings'); ?>"><span> <?php echo lang('settings');?></span></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url('admin/logout'); ?>"><span><i class="fa fa-sign-out"></i> <?php echo lang('logout');?></span></a></li>
					</ul>
					<?php
					}
					if ($this->session->userdata('is_logged_in') AND $this->session->userdata('role') == 2){
					?>
					<ul>
					   <li><a href="<?php echo base_url('admin/dashboard');?>"><span class="<?php echo $dashactive;?>"><i class="fa fa-tachometer"></i> Dashboard</span></a></li>
					   <li><a href="<?php echo base_url('#'); ?>"><span class="<?php echo $pageactive;?>"><i class="fa fa-files-o"></i> <?php echo lang('pages');?></span></a>
						  <ul>
							 <li><a href="<?php echo base_url('admin/pages'); ?>"><span> <?php echo lang('manage_pages');?></span></a></li>
							 <li><a href="<?php echo base_url('admin/new-page'); ?>"><span> <?php echo lang('new_page');?></span></a></li>
						  </ul>
					   </li>
					   <li><a href="<?php echo base_url('users/mysettings');?>"><span class="<?php echo $mysettingsactive;?>"><i class="fa fa-user"></i> <?php echo lang('mysettings');?></span></a>
					   <li><a href="<?php echo base_url('admin/#'); ?>"><span class="<?php echo $modulesactive;?>"><i class="fa fa-puzzle-piece"></i> Modules</span></a>
                            <ul>
                                <li><a href="<?php echo base_url('admin/modules'); ?>"><span> <?php echo lang('manage_modules');?></span></a></li>
                                <?php foreach($modules as $module){
                                    if($module->mod_active == 1){
                                        echo '<li><a href="'.base_url("admin/modules/edit?mod=").$module->mid.'"><span>'.$module->mod_name.'</span></a></li>';   
                                    }
                                    else{}
                                }?>
                                <li><a href="<?php echo base_url("admin/#"); ?>"><span> <?php echo lang('new_module');?></span></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url('admin/logout'); ?>"><span><i class="fa fa-sign-out"></i> <?php echo lang('logout');?></span></a></li>
					</ul>
					<?php
						}
					?>
				</div>
			</nav>
			<div id="info">
				<img class="sidebar-logo" src="<?php echo base_url('assets/images/logo.png');?>" />
		        <a href="#">Simplicity</a> &copy; <?php echo date('Y'); ?><br>
		 		<a href="#">Contact</a> &middot;  <a href="#">Cookies</a>
      		</div>
		</div>
        <script src="<?php echo base_url('assets/js/menu.js'); ?>"></script> <!-- Include menu js -->