<script>
  $( document ).ready(function() {
    $('#sort').sortable({
	placeholder: "ui-state-highlight",
  
  update: function(evt, ui) {  
    $.ajax({
      type: 'POST',
      url: '<?php echo base_url('page/save_page_order');?>',
      dataType: 'json',
      data: $('#sort').sortable('serialize'),
      success: function(msg) {
      }
    });
  }
}); 
});
  </script>

<div id="content">
    	<div id="innerContent">
		<h1><?php echo lang('page_h1_title');?></h1>
            <div class="topMenu">
            <button class='btn btn-success' onClick="location.href='<?php echo base_url("admin/new-page"); ?>'"><?php echo lang('new_page'); ?></button>
            <div class="topMenu">
<?php if ($this->session->flashdata('success')){
					echo '<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="fa fa-check"></i> <strong>'.lang('success').'</strong> ' . $this->session->flashdata('success') . ' 
							</div>'; } ?>
							
			<?php if ($this->session->flashdata('errors')){
					echo '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="fa fa-times"></i> <strong>'.lang('error').'</strong> ' . $this->session->flashdata('errors') . ' 
					</div>'; } ?>
<ul id="sort">
<?php
$html = '';
    if($pages){
        foreach($pages as $row){
            $html .= '<li id="s_'.$row->p_id .'" class="ui-state-default">';
            $html .= '<a class="sortable-li-padding page-overview-title" href="'.base_url("admin/pages/edit-page?id=").$row->p_id .'">'. $row->page_title .'</a>';
            $html .= '<a href="delete_page?pid='.$row->p_id.'"><button class="btn btn btn-danger pages-button">'. lang('page_overview_delete') .'</button></a>';
            $html .= '<div class="page-options"><a href="pages/edit-page?id='.$row->p_id.'">'.lang('page_overview_edit').'</a> | <a href="'.base_url($row->page_title).'">'.lang('page_overview_view').'</a></div>';
            $html .= '</li>';
        }
    }
    else{
        echo lang("page_nopage");
    }
$html .= '</ul></div></div>';
echo $html;
?>