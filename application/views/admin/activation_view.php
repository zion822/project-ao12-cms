<?php
$html = '<!DOCTYPE html> 
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="'. base_url('assets/favicon.png') .'"/>

    <title>Activation - Simplicity</title>

    <!-- Bootstrap core CSS -->
    <link href="' . base_url('assets/css/bootstrap.css') .'" rel="stylesheet" type="text/css" />
    <link href="' . base_url('assets/css/login.css') .'" rel="stylesheet" type="text/css" />

     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="login-container">';

    $attributes = array('class' => 'form-signin', 'name' => 'login', 'autocomplete' => 'off');
    $password = array('name' => 'password', 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'Password');
    $passwordCheck = array('name' => 'passwordcheck', 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'Confirm password');
    $submit = array('name' => 'login_submit', 'value' => 'Activate', 'class' => 'submit-button');
    
    $html .= form_open('admin/activationValidation', $attributes);
    $html .=    "<h2 class='form-signin-heading'>Activation</h2><p>Please fill in the password that you will use for this account</p>";
                if($this->session->flashdata('errors')){
                $html .= '  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="fa fa-times"></i> <strong>'.lang('error').'</strong> ' . $this->session->flashdata('errors') . '</div>';
    };
    $html .= validation_errors();

    $html .= form_password($password);
    $html .= form_password($passwordCheck);

    $html .= form_submit($submit);

    $html .= form_close();
    

    $html .= '
    
    </div> <!-- /container -->
    
    <div class="logo">
    <img class="login-logo" src="' . base_url('assets/images/logo.png') .'" />
    </div>
    

  </body>
</html>';
echo $html;
?>