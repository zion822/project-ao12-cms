<?php
    foreach ($module as $row){
        $mod_name = $row->mod_name;
        $mod_author = $row->mod_author;
        $mod_description = $row->mod_description;
    }
?>
<div id="content">
	<div id="innerContent">
		<h1><?php echo $mod_name; ?></h1>
        <div id="usersManagement">
		<div class="topMenu">
			<?php echo 'Created by: '.$mod_author;?>
		</div>
		<?php include("/assets/modules/".$mod_name."/".$mod_name.".php");?>
        </div>
    </div>
</div>