<?php
$id = $this->input->get('id');
foreach($pages as $row){
	$page_title = $row->page_title;
	$content = $row->content;
}
?>
    <!--
	WYSIWYG Editor
	-->
<script type="text/javascript" src="<?php echo base_url('assets/js/tinymce/tinymce.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/dropzone/dropzone.js'); ?>"></script>
<?php echo link_tag("assets/js/dropzone/css/dropzone.css"); ?>
<script type="text/javascript">
    tinymce.init({
        theme: "modern",
        skin: "light",
        selector: "textarea",
        relative_urls:false,
        height: 450,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor",
        ],
        toolbar1: "forecolor bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent link image undo redo",
        image_advtab: true,
        menubar: false
    });
            
    
</script>
<!--
	END WYSIWYG Editor
	-->

<div id="content">
    <div id="innerContent">
        <h1><?php echo $page_title; ?></h1>
			<?php if ($this->session->flashdata('error')){
			echo '<strong>'.lang('error').'</strong> ' . $this->session->flashdata('error'); } 
					
					if ($this->session->flashdata('success')){
					echo '<strong>'.lang('success').'</strong> ' . $this->session->flashdata('success'); }
					
			?>
			<?php
				    $attributes = array('class' => 'form-horizontal', 'accept-charset=' => 'UTF-8', 'role' => 'form');
					echo form_open('admin/update_page', $attributes);			
					$titleatt = array('name' => 'title', 'class' => 'form-control', 'value' => $page_title);
					$contentatt = array ('name' => 'content', 'value' => $content); ?>
					
					
				<div class='control-group'>
				    <label class='control-label' for='Page title'><?php echo lang('edit_page_title');?></label>

				    <div class='controls'>
				        <?php echo form_input($titleatt); ?>
				    </div>
                </div>

				<div class='control-group'>
				    <label class='control-label' for='content'><?php echo lang('edit_page_content');?></label>
                    <div class='add-media' id="media"><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-folder-open"></span> &nbsp; Default</button></div>
				    <div class='controls'>
				        <?php echo form_textarea($contentatt); ?>
				    </div>
                </div>
						  
				<div class='control-group'>
				    <label class='control-label' for='update_page'></label>
				    <div class='controls'>
				        <button id='update_page' name='update_page' class='btn btn-success'><?php echo lang('edit_page_save');?></button>
				    </div>
                </div>
				<?php echo form_hidden('id', $id);
				echo form_close(); ?>

        <div id="upload-dialog">
            <div class="upload-dialog-heading">
                <div class="upload-dialog-close"><span class="glyphicon glyphicon-remove"></span></div>
                <h1>Media</h1>
                <ul class="nav nav-tabs" role="tablist">
                  <li class="active"><a href="#upload" role="tab" data-toggle="tab">Upload</a></li>
                  <li><a href="#library" role="tab" data-toggle="tab">Media library</a></li>
                </ul>
            </div>
            <div class="upload">
                <!-- Nav tabs -->
                

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="upload">
                        <?php $form_attributes = array('class' => 'dropzone');
                        echo form_open_multipart('files/upload', $form_attributes);
                        echo '<div class="dropzone-previews"></div>'; //this is were the previews should be shown
                        echo form_close();?>   
                    </div>
                    <div class="tab-pane" id="library">...</div>
                </div>
                             
            </div>
        </div>
    </div><!-- innercontent -->
</div><!-- content -->
<script>
$( "#media" ).click(function() {
    $( "#upload-dialog" ).css("display", "block");
    $( "#navPosition" ).css("opacity", "0.5");
    $( "#topBar" ).css("opacity", "0.5");
});
    
$( ".upload-dialog-close" ).click(function(){
    $( "#upload-dialog" ).removeAttr('style');
    $( "#navPosition" ).removeAttr('style');
    $( "#topBar" ).removeAttr('style');
});
</script>