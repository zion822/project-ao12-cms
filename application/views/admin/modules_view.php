<div id="content">
	<div id="innerContent">
		<h1>Modules</h1>
        <div class="topMenu">
        <button class='btn btn-success' onClick="location.href='<?php echo base_url("admin/modules/#"); ?>'"><?php echo lang('new_module'); ?></button>
        <div class="topMenu">
        <?php if ($this->session->flashdata('success')){
					echo '<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="fa fa-check"></i> <strong>Success!</strong> ' . $this->session->flashdata('success') . ' 
							</div>'; } ?>
							
			<?php if ($this->session->flashdata('errors')){
					echo '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="fa fa-times"></i> <strong>Error</strong> ' . $this->session->flashdata('errors') . ' 
					</div>'; } ?>
        <div id="usersManagement">

		<table id="table_data" class="table table-striped">
			<th><?php echo lang('action');?></th><th><?php echo lang('mod_name');?></th><th><?php echo lang('description');?></th><th><?php echo lang('author');?></th>
            <tr>
            <?php
                foreach ($modules as $row){
                    if($row->mod_active == 0){echo '<td><a href="'.base_url('admin/modules/activate?mod=').$row->mid.'">Activate</a></td>';}
                    else{echo'<td><a href="'.base_url('admin/modules/deactivate?mod=').$row->mid.'">Deactivate</a>';}
                    //echo '<td>Activate</td>';
                    echo '<td>'.$row->mod_name.'</td>';
                    echo '<td>'.$row->mod_description.'</td>';
                    echo '<td>'.$row->mod_author.'</td>';
                }
?>
            </tr>
            </table>
        </div>
    </div>
</div>