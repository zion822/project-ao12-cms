
<div id="content">
	<div id="innerContent">
		<h1><?php echo lang('addusertitle'); ?></h1>
		<div id="usersManagement">
			    <?php
                if ($this->session->flashdata('errors')){
                    echo '  <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <i class="fa fa-times"></i> <strong>'.lang('error').'</strong> ' . $this->session->flashdata('errors') . ' 
                            </div>';
                }

                $attributes = array('class' => 'form-pdf', 'name' => 'login', 'autocomplete' => 'off');
                $name       = array('name' => 'firstname', 'required' => 'required', 'class' => 'form-control', 'placeholder' => '','value' => $this->session->flashdata('firstname'));
                $lastname   = array('name' => 'lastname', 'required' => 'required', 'class' => 'form-control', 'placeholder' => '', 'value' => $this->session->flashdata('lastname'));
                $email      = array('name' => 'email', 'required' => 'required', 'class' => 'form-control', 'placeholder' => '', 'value' => $this->session->flashdata('email'));
                $password   = array('name' => 'pwd', 'required' => 'required', 'class' => 'form-control', 'placeholder' => '', 'value' => '');
                $passwordRep   = array('name' => 'pwdRep', 'required' => 'required', 'class' => 'form-control', 'placeholder' => '', 'value' => '');
                $options    = array('1' => 'Admin','2' => 'User');
                $submit     = array('name' => 'login_submit', 'value' => ''.lang('addbutton').'', 'class' => 'btn btn-lg btn-success btn-block');
                
                echo form_open('users/adduser_validation', $attributes);
                echo validation_errors(); ?>

                <label class="control-label" for="Name"><?php echo lang('name'); ?></label>
                    <?php echo form_input($name); ?>
                <label class="control-label" for="Last name"><?php echo lang('lastname'); ?></label>
                    <?php echo form_input($lastname); ?>
                <label class="control-label" for="Email"><?php echo lang('email'); ?></label>
                    <?php echo form_input($email); ?>
                <label class="control-label" for="Email"><?php echo lang('userlevel'); ?></label>
                    <?php echo form_dropdown('userlvl', $options, '1', 'class="form-control"');
                echo '<br>';
                echo form_submit($submit);
                echo form_close();
            ?>
		</div>
	</div>
</div>
