<div id="content">
        
        <div id="innerContent">
      
            <h1>Dashboard</h1>
            <div class="dashBlock">
                <div class="dashBlockHeading">
                    <?php echo lang('recent_activity');?>
                </div>
                <div class="dashBlockContent">
                 <?php foreach($logs as $row){
                    echo '<div class="dashBlockRow"><i class="';
                     if($row->type == 1){
                        echo 'fa fa-check-circle-o"></i> ';   
                     }
                     else if($row->type == 0){
                         echo 'fa fa-exclamation-triangle"></i> ';
                     }
                        echo $row->description.' '.$row->username.' on '.$row->datetime .'</div>';
                    }?>
                    
                </div>
            </div><!-- .dashBlock -->
            <div class="dashBlock">
                <div class="dashBlockHeading">
                    <?php echo lang('statistics');?>
                </div>
                <div class="dashBlockContent">
                    <div class="dashBlockRow">
                        <div class="dashBlockRowLeft">
                            <?php echo lang('simplicity_version');?>
                        </div>
                        <div class="dashBlockRowRight">
                            1.0
                        </div>
                        <div class="dashBlockRowLeft">
                            <?php echo lang('php_version');?>    
                        </div>
                        <div class="dashBlockRowRight">
                            <?php echo phpversion();?>
                        </div>
                    </div>
                </div>
            </div><!-- .dashBlock">-->
        </div><!-- .innerContent -->
	</div><!-- .content -->

