<div id="content">
	<div id="innerContent">
		<h1><?php echo lang('editusertitle'); ?></h1>
		<div id="usersManagement">
			    <?php
                if($this->session->flashdata('success')){

                    echo '  <div id="error">
                                <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <i class="fa fa-check"></i> <strong>'.lang('success').'</strong> ' . $this->session->flashdata('success') . '
                                </div>
                            </div>'; 
                }

                if ($this->session->flashdata('errors')){
                    echo '  <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <i class="fa fa-times"></i> <strong>'.lang('error').'</strong> ' . $this->session->flashdata('errors') . ' 
                            </div>';
                }

                $attributes = array('class' => 'form-pdf', 'name' => 'login', 'autocomplete' => 'off');
                $name       = array('name' => 'firstname', 'required' => 'required', 'class' => 'form-control', 'value' => $user['firstname']);
                $lastname   = array('name' => 'lastname', 'required' => 'required', 'class' => 'form-control', 'value' => $user['lastname']);
                $email      = array('name' => 'email', 'required' => 'required', 'class' => 'form-control', 'value' => $user['email']);
                $options    = array('1' => 'Admin','2' => 'User');
                $submit     = array('name' => 'login_submit', 'value' => ''.lang('changeuser').'', 'class' => 'btn btn-lg btn-success');
                $hidden     = array('uid' => $_GET['u'], 'omail' => $user['email']);

                echo form_open('users/edituser_validation', $attributes, $hidden);
                echo validation_errors(); ?>
                    <label class="control-label" for="Name"><?php echo lang('name'); ?></label>
                        <?php echo form_input($name); ?>
                    <label class="control-label" for="Lastname"><?php echo lang('lastname'); ?></label>
                        <?php echo form_input($lastname); ?>
                    <label class="control-label" for="Email"><?php echo lang('email'); ?></label>
                        <?php echo form_input($email);?>
                    <label class="control-label" for="Role"><?php echo lang('role'); ?></label>
                        <?php echo form_dropdown('userlvl', $options, ''.$user['role'].'', 'class="form-control"' );
                echo '<br>';
                echo form_submit($submit);
                echo form_close();
            ?>
		</div>
	</div>
</div>
