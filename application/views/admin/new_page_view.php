<!--
WYSIWYG Editor
-->
<script type="text/javascript" src="<?php echo base_url('assets/js/tinymce/tinymce.min.js'); ?>"></script>
<script type="text/javascript">
     tinymce.init({
        theme: "modern",
        skin: "light",
        selector: "textarea",
        relative_urls:false,
        height: 450,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor",
        ],
        toolbar1: "forecolor bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent link undo redo",
        menubar: false
    });
</script>
<!--
END WYSIWYG Editor
-->

<div id="content">
    <div id="innerContent">
        <?php if ($this->session->flashdata('success')){
                echo '<strong>Success!</strong> ' . $this->session->flashdata('success'); } ?>

        <?php
                if ($this->session->flashdata('errors')){
                    echo '  <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <i class="fa fa-times"></i> <strong>The following errors occured:</strong> ' . $this->session->flashdata('errors') . ' 
                            </div>';
                }

        $attributes = array('class' => 'form-horizontal', 'accept-charset=' => 'UTF-8', 'role' => 'form');
        echo form_open('page/new_page_validation', $attributes);			
        $titleatt = array('name' => 'title', 'placeholder' => 'Page title', 'class' => 'form-control', 'value' => $this->session->flashdata('title'));
        $contentatt = array ('name' => 'content', 'value' => $this->session->flashdata('content')); ?>

        <div class='control-group'>
            <label class='control-label' for='Page title'>Page title</label>
            <div class='controls'>
                <?php echo form_input($titleatt); ?>
                <p class='help-block'></p>
            </div>
        </div>

        <div class='control-group'>
            <label class='control-label' for='content'>Content</label>
            <div class='controls'>
                <?php echo form_textarea($contentatt); 
                form_error('content', '<div id="error"><img src='. base_url("assets/images/error.png") . ' alt="error" width="15" height="15"> ' , '</div>'); ?>
            </div>
        </div>

        <div class='control-group'>
            <label class='control-label' for='new_page_submit'></label>
            <div class='controls'>
                <button id='new_page_submit' name='new_page_submit' class='btn btn-success'>Create new page</button>
            </div>
        </div>
    </div><!-- #innerContent -->
</div><!-- #content -->
    

<?php echo form_close(); ?>