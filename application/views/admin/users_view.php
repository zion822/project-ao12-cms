<script type='text/javascript' language='javascript'>

  $( document ).ready(function(){
      //Remove friend ajax request
      $( ".removeFriend" ).click(function() {
        var profileid = $(this).attr("data-profileid");

     	$.ajax({
            url: '<?php echo base_url().'users/removeUser';?>',
            type:'POST',
            data: ({u_id: profileid}),
            dataType: 'json',
            success: removeUser() // End of success function of ajax form
            }); // End of ajax call

        function removeUser(){
          $(".removeFriend[data-profileid='" + profileid + "']").closest("tr").fadeOut("slow");
          $(this).remove();
        }
      });
  });
</script>
<div id="content">
	<div id="innerContent">
		<h1><?php echo lang('users'); ?></h1>
	
	<div id="usersManagement">
		<div class="topMenu">
			<button class='btn btn-success' onClick="location.href='<?php echo base_url("users/adduser"); ?>'"><?php echo lang('adduserbutton'); ?></button>
		</div>
		<table id="table_data" class="table table-striped">
			<th><?php echo lang('name'); ?></th><th><?php echo lang('lastname'); ?></th><th><?php echo lang('email'); ?></th><th><?php echo lang('role'); ?></th><th><?php echo lang('active'); ?></th><th><?php echo lang('changeuser'); ?></th><th><?php echo lang('removeuser'); ?></th>
			<?php
			foreach($usersDetails as $singlUser){

        switch($singlUser->role){
          case 1:
            $roleLevel=lang('admin');
            break;
          case 2:
            $roleLevel=lang('beheerder');
            break;
        }

        switch ($singlUser->active) {
          case 0:
            $active = lang('no');
            break;
          case 1:
            $active = lang('yes');
        }
       
      
      $id =  $this->session->userdata('u_id');
      if($id == $singlUser->u_id){
          $d = 'disabled="disabled"';
          $c = 'btndisable';
      }else{
          $d = "";
          $c = "";
     }
			echo'<tr>
            <td>'.$singlUser->firstname.'</td>
            <td>'.$singlUser->lastname.'</td>
            <td><a href="mailto:"'.$singlUser->email.'">'.$singlUser->email.'</a></td>
            <td>'.$roleLevel.'</td>
            <td>'.$active.'</td>
            <td>
            <a href="'.base_url("users/edituser").'/?u='.$singlUser->u_id.'">
            <button class="btn btn-warning">'.lang('changeuser').'</button>
            </a>
            </td>
            <a href="#">
            <td><button class="removeFriend btn btn-danger" '.$d.' data-profileid="'.$singlUser->u_id.'">'.lang('removeuser').'</button></td>
            </a>
				</tr>';
			}?>
		</table>
	</div>
	</div>
</div>