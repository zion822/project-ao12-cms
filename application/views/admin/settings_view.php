<?php
// Settings
$site_title = $settings[0]->setting_value;
$site_slogan = $settings[1]->setting_value;
$admin_email = $settings[2]->setting_value;
?>

<div id="content">
    <div id="innerContent">
    <h1>Settings</h1>
        <?php if ($this->session->flashdata('success')){
            echo '<div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-check"></i> <strong>Success!</strong> ' . $this->session->flashdata('success') . ' 
                    </div>'; } ?>

        <?php if ($this->session->flashdata('errors')){
            echo '<div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="fa fa-times"></i> <strong>'. lang('errors').'</strong> ' . $this->session->flashdata('errors') . ' 
            </div>'; }
        
        $attributes = array('class' => 'form-horizontal', 'accept-charset=' => 'UTF-8', 'role' => 'form');
        echo form_open('settings/settings_update', $attributes);			
		$title = array('name' => 'site_title', 'value' => $site_title, 'class' => 'form-control');
		$slogan = array('name' => 'site_slogan', 'value' => $site_slogan, 'class' => 'form-control');
		$email = array('name' => 'admin_email', 'value' => $admin_email, 'class' => 'form-control');
        ?>
        
        <!--    The form    -->
        <div class='control-group'>
            <label class='control-label' for='Site title'><?php echo lang('settings_title');?></label>
            <div class='controls'>
                <?php echo form_input($title); ?>
            </div>
        </div>				
						 
        <div class='control-group'>
		    <label class='control-label' for='Site slogan'><?php echo lang('settings_slogan');?></label>
            <div class='controls'>
				<?php echo form_input($slogan); ?>
				<p class='help-block'><?php echo lang('settings_slogan_help');?></p>
            </div>
        </div>	
        
        <div class='control-group'>
            <label class='control-label' for='Admin email'><?php echo lang('settings_email');?></label>
            <div class='controls'>
                <?php echo form_input($email); ?>
                <p class='help-block'><?php echo lang('settings_email_help'); ?></p>
            </div>
        </div>

        <div class='control-group'>
            <label class='control-label' for='submit'></label>
            <div class='controls'>
                <button id='submit_settings' name='submit_settings' class='btn btn-success'>Save changes</button>
            </div>
        </div>
		<?php echo form_close(); ?>
    </div> <!-- #innetContent -->
</div> <!-- #content -->