<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';
$route['admin/users'] = "users";
$route['admin/users/edituser'] = "users/edituser";
$route['admin/pages'] = "page";
$route['admin/new-page'] = "page/new_page";
$route['admin/pages/edit-page'] = "page/edit_page";
$route['admin/update_page'] = "page/update_page";
$route['admin/delete_page'] = "page/delete_page";
$route['admin/settings'] = "settings";
$route['admin/modules/activate'] = "modules/activate";
$route['admin/modules/deactivate'] = "modules/deactivate";
$route['admin/modules/edit'] = "modules/edit_module";
$route['admin/modules'] = "modules";

$controller_dir = opendir(APPPATH."controllers");

while (($file = readdir($controller_dir)) !== false) {

    if (substr($file, -4) == ".php" ) {

        $route[substr($file, 0, -4)."(.*)"] = substr($file, 0, -4)."$1";

    } elseif (substr($file, -5) == ".php/") {

        $route[substr($file, 0, -5)."(.*)"] = substr($file, 0, -5)."$1";

    }
}

$route["(:any)"] = "home/index";
/* End of file routes.php */
/* Location: ./application/config/routes.php */