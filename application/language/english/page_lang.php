<?php
    $lang['page_nopage'] = 'It seems like there is no page available. <a href="http://localhost/simplicity/admin/new-page">Create one now.</a>';
    $lang['page_h1_title'] = 'Pages';
    $lang['page_overview_edit'] = 'Edit';
    $lang['page_overview_view'] = 'View';
    $lang['page_overview_delete'] = 'Delete';

    $lang['edit_page_title'] = 'Page title';
    $lang['edit_page_content']  = 'Content';
    $lang['edit_page_save'] = 'Save changes';

    $lang['page_error_notfound'] = 'Page could not be found.';
?>