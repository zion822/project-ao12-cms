<?php
    $lang['name'] 			= 'Name';
    $lang['lastname'] 		= 'Lastname';
    $lang['email'] 			= 'E-mail';
    $lang['userlevel'] 		= 'Role level';
    $lang['addusertitle'] 	= 'Add user';
    $lang['adduserbutton'] 	= 'Add new users';
    $lang['addbutton'] 		= 'Add';
    $lang['users'] 			= 'Users';
    $lang['role'] 			= 'Role';
    $lang['changeuser'] 	= 'Change user';
    $lang['removeuser'] 	= 'Delete user';
    $lang['admin'] 			= 'Administrator';
    $lang['beheerder'] 		= 'Writer';
    $lang['editusertitle'] 	= 'Edit user';
    $lang['active']         = 'Active';
    $lang['yes']         = 'yes';
    $lang['no']         = 'no';
?>