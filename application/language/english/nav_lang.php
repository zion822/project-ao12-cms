<?php
    $lang['pages'] = 'Pages';
    $lang['manage_pages'] = 'Manage pages'; 
    $lang['new_page'] = 'New page';
    $lang['users'] = 'Users';
    $lang['manage_users'] = 'Manage users';
    $lang['user_permissions'] = 'Permissions';
    $lang['mysettings'] = 'My settings';
    $lang['manage_modules'] = 'Manage modules';
    $lang['new_module'] = 'New module';
    $lang['system'] = 'System';
    $lang['settings'] = 'Settings';
    $lang['logout'] = 'Logout';

    $lang['error'] = 'The following errors occured:';
    $lang['success'] = 'Success!';
?>