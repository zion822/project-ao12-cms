<?php
    $lang['errors'] = 'Something went wrong.';

    $lang['settings_title'] = 'Site title';
    $lang['settings_slogan'] = 'The website\'s slogan';
    $lang['settings_slogan_help'] = 'Enter a few keywords about the site.';
    $lang['settings_email'] = 'E-mail Address';
    $lang['settings_email_help'] = 'This E-mail address will be used for admin purposes.';
// language dropdown
    $lang['settings_language'] = 'Language';
    $lang['settings_language_en'] = 'English';
    $lang['settings_language_nl'] = 'Dutch';
?>