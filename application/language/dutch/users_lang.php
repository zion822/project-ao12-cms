<?php
    $lang['name'] 			= 'Naam';
    $lang['lastname'] 		= 'Achternaam';
    $lang['email'] 			= 'E-mail';
    $lang['userlevel'] 		= 'Rol gebruiker';
    $lang['addusertitle'] 	= 'Gebruiker toevoegen';
    $lang['adduserbutton'] 	= 'Nieuwe gebruiker toevoegen';
    $lang['addbutton'] 		= 'Gebruiker toevoegen';
    $lang['users'] 			= 'Gebruikers';
    $lang['role'] 			= 'rol';
    $lang['changeuser'] 	= 'Gebruiker wijzigen';
    $lang['removeuser'] 	= 'Gebruiker verwijderen';
    $lang['admin'] 			= 'Administrator';
    $lang['beheerder'] 		= 'schrijver';//writer
    $lang['editusertitle'] 	= 'Wijzig gebruiker';
    $lang['active']         = 'Actief';
    $lang['yes']         = 'Ja';
    $lang['no']         = 'Nee';
    

?>