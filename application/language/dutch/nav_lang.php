<?php
    $lang['pages'] = 'Pagina\'s';
    $lang['manage_pages'] = 'Pagina beheer'; 
    $lang['new_page'] = 'Nieuwe pagina';
    $lang['users'] = 'Gebruikers';
    $lang['manage_users'] = 'Gebruikers beheer';
    $lang['user_permissions'] = 'Rechten';
    $lang['mysettings'] = 'Mijn instellingen';
    $lang['manage_modules'] = 'Module beheer';
    $lang['new_module'] = 'Nieuwe module';
    $lang['system'] = 'Systeem';
    $lang['settings'] = 'Instellingen';
    $lang['logout'] = 'Uitloggen';

    $lang['error'] = 'Er is een fout opgetreden:';
    $lang['success'] = 'Gelukt!';
?>