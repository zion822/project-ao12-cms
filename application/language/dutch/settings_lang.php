<?php
    $lang['errors'] = 'Er is een fout opgetreden.';

    $lang['settings_title'] = 'De naam van de website';
    $lang['settings_slogan'] = 'De slogan van de website';
    $lang['settings_slogan_help'] = 'Voer een beschrijving in over de website.';
    $lang['settings_email'] = 'E-mail Adres';
    $lang['settings_email_help'] = 'Dit E-mail adres wordt gebruikt voor admin doeleinden.';
// talen dropdown
    $lang['settings_language'] = 'Taal';
    $lang['settings_language_en'] = 'Engels';
    $lang['settings_language_nl'] = 'Nederlands';
?>