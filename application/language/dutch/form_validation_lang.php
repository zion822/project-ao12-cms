<?php

$lang['required']			= "%s is verplicht.";
$lang['isset']				= "%s moet een waarde hebben.";
$lang['valid_email']		= "%s is geen geldig e-mail address.";
$lang['valid_emails']		= "%s zijn geen geldige e-mail addressen.";
$lang['valid_url']			= "%s is geen geldige url.";
$lang['valid_ip']			= "%s is geen geldig IP address.";
$lang['min_length']			= "%s moet tenminste %s characters lang zijn.";
$lang['max_length']			= "%s mag niet langer zijn dan %s characters.";
$lang['exact_length']		= "%s moet precies %s characters lang zijn.";
$lang['alpha']				= "%s mag enkel letters bevatten [a-Z].";
$lang['alpha_numeric']		= "%s mag enkel letters [a-Z] en cijfers bevatten [0-9].";
$lang['alpha_dash']			= "%s mag enkel letters [a-Z], cijfers [0-9] en streepjes bevatten.";
$lang['numeric']			= "%s mag enkel cijfers bevatten.";
$lang['is_numeric']			= "%s mag enkel  numerieke characters bevatten.";
$lang['integer']			= "%s moet een integer zijn.";
$lang['regex_match']		= "%s is geen correct tijd formaat.";
$lang['matches']			= "%s is niet gelijk aan %s.";
$lang['is_unique'] 			= "%s moet uniek zijn.";
$lang['is_natural']			= "%s mag enkel positieve getallen bevatten.";
$lang['is_natural_no_zero']	= "%s moet een nummer hoger dan 0 zijn.";
$lang['decimal']			= "%s mag enkel een decimaal getal zijn.";
$lang['less_than']			= "%s moet kleiner zijn dan. %s.";
$lang['greater_than']		= "%s moet groter zijn dan. %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/dutch/form_validation_lang.php */