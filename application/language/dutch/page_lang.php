<?php
    $lang['page_nopage'] = 'Er is nog geen pagina. <a href="http://localhost/simplicity/admin/new-page">Maak er nu een aan.</a>';
    $lang['page_h1_title'] = 'Pagina beheer';
    $lang['page_overview_edit'] = 'Bewerken';
    $lang['page_overview_view'] = 'Bekijken';
    $lang['page_overview_delete'] = 'Verwijderen';

    $lang['edit_page_title'] = 'Pagina titel';
    $lang['edit_page_content']  = 'Inhoud';
    $lang['edit_page_save'] = 'Wijzigingen opslaan';

    $lang['page_error_notfound'] = 'Pagina kon niet gevonden worden.';
?>