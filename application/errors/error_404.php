<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>
<style type="text/css">

::selection{ background-color: #E13300; color: white; }
::moz-selection{ background-color: #E13300; color: white; }
::webkit-selection{ background-color: #E13300; color: white; }

body {
    font-family:Arial, Helvetica, sans-serif;
}
p{
    font-size:30px;
    color:#444;
    margin:auto;
    text-align:center;
}
a {
	color: #5AA3D6;
	background-color: transparent;
	font-weight: normal;
    text-decoration:none;
}

h1 {
    font-size:200px;
	color: #444;
}
#error404{
    margin:auto;
    text-align:center;
}
#wrong{
    font-size:50px;
    font-weight:bold;
    color:#444;
    margin:0 auto 50px;
    text-align:center;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin-top:50px;
}
</style>
</head>
<body>
	<div id="container">
        <div id="error404">
		  <h1>404</h1>
        </div>
        <div id="wrong">
            I am so sorry! :(
        </div>
        <p>
            The page you were looking for could not be found.
        </p>
        <p>
            Return to the <a href='<?php echo base_url();?>'>homepage</a>.
        </p>
		</div>
</body>
</html>