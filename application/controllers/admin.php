<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	// Front-page controller
class Admin extends CI_Controller {
    public function __construct() {
        parent::__construct();  
            $this->load->helper('cookie');
            $this->load->model('settings_model');
            $this->load->model('modules_model'); // required for nav
            $language = $this->settings_model->get_language();
            $this->lang->load("nav", $language);
            $this->lang->load("dashboard", $language); // load correct language file based on db setting. Default is english.
        
        // check if install directory exists
        $dir = getcwd();
        if(is_dir($dir.'/install/')){
            unlink($dir.'/install/index.php');
            unlink($dir.'/install/verify.php');
            unlink($dir.'/install/simplicity.sql');
            rmdir($dir.'/install/');
        }
    }
	// Load login page as default (index)
	public function index(){
		if ($this->session->userdata('is_logged_in')){
			$this->dashboard();
		}
		else{
			$this->login();
		}
	}
	// Load login view
	public function login(){
    $data['email'] = $this->input->cookie('rememberme', TRUE);
		$this->load->view('admin/login_view', $data);
	}
	
	// Validate and clean user input
	public function login_validation(){

    $this->load->model('log_model');
		$this->load->library('form_validation');

		
		$this->form_validation->set_rules('username', 'Email', 'required|trim|xss_clean|callback_is_active|callback_validate_credentials|');
		$this->form_validation->set_rules('password', 'Password', 'required|sha1|trim');
		// If form validation succeeded, create session with userdata
		if($this->form_validation->run()){
    
      $data = array (
				'username' => $this->input->post('username'),
				'is_logged_in' => 1
			);

      if(isset($_POST['rememberme'])){

          $cookie = array(
              'name'   => 'rememberme',
              'value'  => $this->input->post('username'),
              'expire' => '86500'
          );
          $this->input->set_cookie($cookie);
      }

			$this->session->set_userdata($data);
            // Log valid login and insert into database
            $log_data = array(
                'type' => '1',
                'description' => 'Successful login',
                'username' => $this->input->post('username'),
                'datetime' => date("d-m-Y, H:i")
            );
            $this->log_model->log_login($log_data);
            
			redirect('admin/dashboard');
		}
		else{
            // Log invalid login and insert into database
            $log_data = array(
                'type' => '0',
                'description' => 'Failed login',
                'username' => $this->input->post('username'),
                'datetime' => date("d-m-Y, H:i")
            );
            $this->log_model->log_login($log_data);
            $this->session->set_flashdata('errors', validation_errors());
			redirect("admin/login");
		}
	}
	
	// Validate credentials in DB
	public function validate_credentials(){
		$this->load->model('users_model');
		
		if($this->users_model->can_log_in()){
			return true;
		}
		else{
			$this->form_validation->set_message('validate_credentials', 'Invalid username/password combination.');
			return false;
		}
	}
	
  public function  is_active(){
    $this->load->model('users_model');
    
    if($this->users_model->user_active()){
      return true;
    }
    else{
      $this->form_validation->set_message('is_active', 'Account is not activated! please check your email.');
      return false;
    }
}
	// logout and return to login page
	public function logout(){
		$this->session->sess_destroy();
		redirect('admin/login');
	}
	
	// The actual admin panel
	public function dashboard(){
		if ($this->session->userdata('is_logged_in')){ // if logged in, proceed
            
            $this->load->model('log_model');
            
            $data['logs'] = $this->log_model->get_logs();
            
			$settings['page_title'] = 'Dashboard &laquo; Simplicity';
            $settings['modules'] = $this->modules_model->get_modules(); // required for nav
			$this->load->view('admin/includes/head', $settings);
			$this->load->view('admin/dashboard_view', $data);
			$this->load->view('admin/includes/footer');
		}
		else{ // Not logged in? Redirect to login page
			$this->login();
		}	
	}
	public function recover(){
       //PAGE SETTINGS
       $settings['page_title'] = "Recover &laquo; Simplicity";

       $this->load->view('admin/recover_view');
     }
    public function recoveryValidate(){
    	   //LOADING LIBRARY FOR FORM VALIDATIOn
          $this->load->library('form_validation');

          //SETTING RULES FOR INPUT
          $this->form_validation->set_rules('email', 'Email', 'valid_email|required|trim|xss_clean|callback_validate_is_email_there');

          //SETTING MESSAGE FOR ERRORS
          $this->form_validation->set_message('required', '<li>%s is required</li>');
          $this->form_validation->set_message('valid_email', '%s address is not valid.');

          // PASSED VALIDATION RUN THIS THIS FUNCTION
          if($this->form_validation->run()){
          		$this->users_model->sentRecoverMail();
               }else{
               //IF VALIDATION FAILS RETURN ERRORS AND SET SESSION DATA FOR LAST INPUTS
               $this->session->set_flashdata('email', $this->input->post('email'));
               $this->session->set_flashdata('errors', validation_errors());
               redirect('admin/recover');
          }
    }

    public function validate_is_email_there(){
    	$this->load->model('users_model');
		if($this->users_model->emailExists()){
			return true;
		}
		else{
			$this->form_validation->set_message('validate_is_email_there', 'This email does not exist in our system.');
			return false;
		}
    }
    public function startRecover(){
    	$this->load->model("users_model");
    	
    	if(isset($_GET['c'])){
    		$recovercode = array(
                   'code'  => $_GET['c']
               );
			$this->session->set_userdata($recovercode);
            	
           	if(!$this->users_model->isRecoverCodeTrue($_GET['c'])){
            	redirect('admin/login');
            }

       		$settings['page_title'] = "Recovery &laquo; Simplicity";
       		$this->load->view('admin/startrecover_view');
    	}else{
    		redirect('admin/login');
    	}
    }

    public function recover_validation(){
    	//LOADIN FORM VALIDATION LIBRARY
        $this->load->library('form_validation');
        $this->load->model("users_model");
        //SETTING FORM VALIDATION RULES

        $this->form_validation->set_rules('password', 'Password Field', 'trim|required');
		$this->form_validation->set_rules('passwordcheck', 'Password check', 'trim|required|callback_check_equal['.$this->input->post('password').']');

          $this->form_validation->set_message('required', '<li>%s is required</li>');

          //IF VALIDATION SUCCEEDED RUN FUNCTION
        if($this->form_validation->run()){
            $this->users_model->setNewPassword();
            redirect('admin/login');
        }else{
               //IF VALIDATION FAILS RETURN ERRORS AND SET SESSION DATA FOR LAST INPUTS
               $this->session->set_flashdata('errors', validation_errors());
               //REDIRECT BACK TO PAGE AND SET ERROR MESSAGE
               redirect('admin/startrecover?c='.$this->session->userdata('code'));
        }
    }

    public function check_equal($password, $passwordcheck){
	    if ($password == $passwordcheck){
	        return true;       
	    }else{
	    	$this->form_validation->set_message('check_equal', 'The passwords do not match!');
	        return false;
	    }
  	}
    public function activation(){
        
        $this->load->model('users_model');

        if(isset($_GET['c'])){
            $recovercode = array(
                                    'emailcode'  => $_GET['c']
                                );
        $this->session->set_userdata($recovercode);
              
        if(!$this->users_model->isEmailTrue($_GET['c'])){
          redirect('admin/login');
        }

     $settings['page_title'] = "Activation &laquo; Simplicity";
     //LOADING ADDUSER_VIEW AND DATA
     $this->load->view('admin/activation_view', $settings);

   }
}
   public function activationValidation(){
      //LOADIN FORM VALIDATION LIBRARY
        $this->load->library('form_validation');
        $this->load->model("users_model");
        //SETTING FORM VALIDATION RULES

    $this->form_validation->set_rules('password', 'Password Field', 'trim|required');
    $this->form_validation->set_rules('passwordcheck', 'Password check', 'trim|required|callback_check_equal['.$this->input->post('password').']');

          $this->form_validation->set_message('required', '<li>%s is required</li>');

          //IF VALIDATION SUCCEEDED RUN FUNCTION
        if($this->form_validation->run()){
            $this->users_model->activeAccount();
            redirect('admin/login');
        }else{
               //IF VALIDATION FAILS RETURN ERRORS AND SET SESSION DATA FOR LAST INPUTS
               $this->session->set_flashdata('errors', validation_errors());
               //REDIRECT BACK TO PAGE AND SET ERROR MESSAGE
               redirect('admin/activation?c='.$this->session->userdata('emailcode'));
        }
    }
}