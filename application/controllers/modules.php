<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Modules extends CI_controller{
    public function __construct() {
    parent::__construct();  
        $this->load->model('settings_model');
        $this->load->model('modules_model'); // required for nav
        $language = $this->settings_model->get_language();
        $this->lang->load("nav", $language); // load correct language file for the menu(nav).
        $this->lang->load("module", $language); // load correct language file based on db setting. Default is english.
        $this->lang->load("dashboard", $language);
}

    public function index(){      

        $data['settings'] = $this->settings_model->get_settings();
        $settings['page_title'] = 'Modules &laquo; Simplicity';
        $settings['modules'] = $this->modules_model->get_modules(); // required for nav
        
        $this->load->view('admin/includes/head', $settings);
        $this->load->view('admin/modules_view', $data);
        $this->load->view('admin/includes/footer');
    }
    public function activate(){
        $mid = $this->input->get('mod', TRUE);
        
        if($this->modules_model->activate($mid)){
            $this->session->set_flashdata('success', 'Module has been activated.'); // Success message if module has been activate.
            redirect('admin/modules');
        }
        else{
            $this->session->set_flashdata('errors', 'Module cannot be activated.'); // Error message if module cannot be deactivated.
            redirect('admin/modules');
        }
        
        
    }
    public function deactivate(){
 
        $mid = $this->input->get('mod', TRUE);
        
        if($this->modules_model->deactivate($mid)){
            $this->session->set_flashdata('success', 'Module has been deactivated.'); // Success message if module has been deactivated.
            redirect('admin/modules');
        }
        else{
            $this->session->set_flashdata('errors', 'Module cannot be deactivated.'); // Error message if module cannot be deactivated.
            redirect('admin/modules');
        }
    }
    
    public function edit_module(){
        $mid = $this->input->get('mod', TRUE);
        $data['settings'] = $this->settings_model->get_settings();
        $data['module'] = $this->modules_model->get_module($mid);
        $settings['page_title'] = 'Modules &laquo; Simplicity';
        $settings['modules'] = $this->modules_model->get_modules(); // required for nav
        
        $this->load->view('admin/includes/head', $settings);
        $this->load->view('admin/module_edit_view', $data);
        $this->load->view('admin/includes/footer');
    }
}
?>