<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends ci_controller{
    public function __construct() {
        parent::__construct();  
        if ($this->session->userdata('is_logged_in')){
            $this->load->model('settings_model');
            $this->load->model('modules_model'); // required for nav
            $language = $this->settings_model->get_language();
            $this->lang->load("nav", $language);
            $this->lang->load("settings", $language); // load correct language file based on db setting. Default is english.
            $this->lang->load("dashboard", $language);
        }
        else{
            redirect('admin/login'); // Not logged in? Redirect to login
        }
    }
    
    public function index(){
        $this->load->model('settings_model');

        $data['settings'] = $this->settings_model->get_settings();
        $settings['modules'] = $this->modules_model->get_modules(); // required for nav

        $settings['page_title'] = 'Settings &laquo; Simplicity';

        $this->load->view('admin/includes/head', $settings);
        $this->load->view('admin/settings_view', $data);
        $this->load->view('admin/includes/footer');
    }
    
    public function settings_update(){
        if($this->session->userdata('is_logged_in')){

            $this->load->library('form_validation');
        
            $this->form_validation->set_rules('site_title', 'Site title', 'required');
                
            if($this->form_validation->run()){
              //load models
                $this->load->model('settings_model');
                if($this->settings_model->update_settings()){
                    $this->session->set_flashdata('success', 'Settings have been updated.');
                    redirect('admin/settings');
                }
                else{
                    $this->session->set_flashdata('errors', 'Oops, something went wrong trying to update the settings.');
                    redirect('admin/settings');
                }
            }
            else{
                $this->session->set_flashdata('errors', validation_errors());
                redirect('admin/settings');
            }
            

        }
    }
    
    public function settings_validation(){
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('site_title', 'Site title', 'required');
    }
    
        
}
?>