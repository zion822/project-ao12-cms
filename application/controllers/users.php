<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Front-page controller
class Users extends CI_Controller {

     public function __construct() {
          parent::__construct();  
          if ($this->session->userdata('is_logged_in')){
               $this->load->model('settings_model');
               $this->load->model('modules_model'); // required for nav
               $language = $this->settings_model->get_language();
               $this->lang->load("nav", $language);
               $this->lang->load("users", $language);
               $this->lang->load("settings", $language); // load correct language file based on db setting. Default is english.
               $this->lang->load("dashboard", $language);
          }else{
               redirect('admin/login'); // Not logged in? Redirect to login
          }
     }

     public function index(){
          if ($this->session->userdata('role') == 1){
               //LOADING MODELS
               $this->load->model('users_model');

               //FEEDING DATA
               $settings['page_title'] = "Manage users &laquo; Simplicity";
               $settings['modules'] = $this->modules_model->get_modules(); // required for nav
               $data['usersDetails'] = $this->users_model->getAllUsers();

               //LOADING VIEWS
               $this->load->view('admin/includes/head', $settings);
               $this->load->view('admin/users_view', $data);
               $this->load->view('admin/includes/footer');
          }else{
               redirect('admin/dashboard');
          }
     }

     public function adduser(){
          if($this->session->userdata('role') == 1){
               //PAGE SETTINGS
               $settings['page_title'] = "Add user &laquo; Simplicity";
               $settings['modules'] = $this->modules_model->get_modules(); // required for nav

               //LOADING HEAD_VIEW AND SETTINGS
               $this->load->view('admin/includes/head', $settings);

               //LOADING MENU ACCORDING TO LEVEL

               //LOADING ADDUSER_VIEW AND DATA
               $this->load->view('admin/adduser_view');

               //LOADING FOOTER
               $this->load->view('admin/includes/footer');
          }else{
               redirect('admin/dashboard');
          }
     }

     public function adduser_validation(){
          //LOADING LIBRARY FOR FORM VALIDATIOn
          $this->load->library('form_validation');

          //SETTING RULES FOR INPUT
          $this->form_validation->set_rules('firstname', 'Name', 'required|trim|xss_clean');
          $this->form_validation->set_rules('lastname', 'Lastname', 'required|trim|xss_clean');
          $this->form_validation->set_rules('email', 'Email', 'valid_email|required|trim|xss_clean|is_unique[users.email]');
          $this->form_validation->set_rules('userlvl', 'Userlvl', 'required|trim|xss_clean');

          //SETTING MESSAGE FOR ERRORS
          $this->form_validation->set_message('validate_adduser_credentials', '%s is already in use.');
          $this->form_validation->set_message('is_unique', '%s is not available');
          $this->form_validation->set_message('required', '<li>%s is required</li>');
          $this->form_validation->set_message('alpha_numeric', '%s can only contain [a-Z] and [0-9].');
          $this->form_validation->set_message('alpha', '%s can only contain letters.');
          $this->form_validation->set_message('valid_email', '%s address is not valid.');

          // PASSED VALIDATION RUN THIS THIS FUNCTION
          if($this->form_validation->run()){
               $this->load->model("users_model");
               $this->users_model->insertUser();
               }else{
               //IF VALIDATION FAILS RETURN ERRORS AND SET SESSION DATA FOR LAST INPUTS
               $this->session->set_flashdata('firstname', $this->input->post('firstname'));
               $this->session->set_flashdata('lastname', $this->input->post('lastname'));
               $this->session->set_flashdata('email', $this->input->post('email'));
               $this->session->set_flashdata('userlvl', $this->input->post('userlvl'));
               $this->session->set_flashdata('errors', validation_errors());
               redirect('users/adduser');
          }
     }

     public function removeUser(){
          //LOOKING FOR U_ID POST FROM AJAX
          if(isset($_POST['u_id']) == true){
               //IF POST ISSEST MAKE VARIABELE
               $uid =  $_POST['u_id'];
          }

          //LOADING USER_MODEL
          $this->load->model("users_model");
          //CALLING FUNCTION FROM USER MODEL WITH POST ID
          $data['uid'] = $this->users_model->removeuser($uid);
     }
     //--------------------------------------------------------- EDIT USER PAGE
     public function edituser(){
          if($this->session->userdata('role') == 1){
               //PAGE SETTINGS
               $settings['page_title'] = "Edit user &laquo; Simplicity";
               $settings['modules'] = $this->modules_model->get_modules(); // required for nav
               //LOADING USER_MODEL DATA
               $this->load->model("users_model");

               //LOADING HEAD
               $this->load->view('admin/includes/head', $settings);

               //RETRIEVING USER ID FROM URL
               $test = $this->input->get('u');
               if(isset($test) == true){
               //IF ISSET ASSIGN TO VARIABELE
               $uid = $_GET['u'];
               }

               //RETRIEVING DATA SINGLE USER WITH $uid SPECIFIC
               $data['user'] = $this->users_model->getUserByiD($uid);

               //LOADING EDITUSER_VIEW
               $this->load->view('admin/edituser_view', $data);

               //LOADING FOOTER
               $this->load->view('admin/includes/footer');
          }else{
               redirect('admin/dashboard');
          }
     }

     //--------------------------------------------------------- EDIT USER VALIDATION FUNCTION
     public function edituser_validation(){
          //LOADIN FORM VALIDATION LIBRARY
          $this->load->library('form_validation');

          //SETTING FORM VALIDATION RULES
          $this->form_validation->set_rules('firstname', 'Firstame', 'required|trim|xss_clean');
          $this->form_validation->set_rules('lastname', 'Lastname', 'required|trim|xss_clean');

          //CHECKING IF OLD EMAIL MACHTING NEW EMAIL
          if($this->input->post('email') == $this->input->post('omail')){
          //TRUE DO NOT LOOK FOR UNIQUE
               $this->form_validation->set_rules('email', 'Email', 'valid_email|required|trim|xss_clean');
          }else{
               //FALSE LOOK IF NEW EMAIL IS IN DB ALREADY
               $this->form_validation->set_rules('email', 'Email', 'valid_email|required|trim|xss_clean|is_unique[users.email]');
          }

          //SETTING VALIDATION ERRORS MESSAGE
          $this->form_validation->set_rules('userlvl', 'Userlvl', 'required|trim|xss_clean');
          $this->form_validation->set_message('validate_adduser_credentials', '%s is already in use.');
          $this->form_validation->set_message('is_unique', '%s is not available');
          $this->form_validation->set_message('required', '<li>%s is required</li>');
          $this->form_validation->set_message('alpha_numeric', '%s can only contain [a-Z] and [0-9].');
          $this->form_validation->set_message('alpha', '%s can only contain letters.');
          $this->form_validation->set_message('valid_email', '%s address is not valid.');

          //IF VALIDATION SUCCEEDED RUN FUNCTION
          if($this->form_validation->run()){
               $this->load->model("users_model");
               $this->users_model->update();

               //CREATING SUCCES MESSAGE
               $this->session->set_flashdata('success', 'The user has been updated.');

               //REDIRECT TO SAME PAGE WITH SUCCES MESSAGE
               redirect('users/edituser?u='.$this->input->post('uid'));
          }else{
               //IF VALIDATION FAILS RETURN ERRORS AND SET SESSION DATA FOR LAST INPUTS
               $this->session->set_flashdata('firstname', $this->input->post('firstname'));
               $this->session->set_flashdata('lastname', $this->input->post('lastname'));
               $this->session->set_flashdata('email', $this->input->post('email'));
               $this->session->set_flashdata('userlvl', $this->input->post('userlvl'));
               $this->session->set_flashdata('errors', validation_errors());
               //REDIRECT BACK TO PAGE AND SET ERROR MESSAGE
               redirect('users/edituser/?u='.$this->input->post('uid'));
          }
     }

     public function mysettings(){
          //LOADING MODELS
          $this->load->model('users_model');

          //FEEDING DATA
          $settings['page_title'] = "My settings &laquo; Simplicity";
          $settings['modules'] = $this->modules_model->get_modules(); // required for nav
          $uid = $this->session->userdata('u_id');
          $data['userDetails'] = $this->users_model->getUserByiD($uid);

          //LOADING VIEWS
          $this->load->view('admin/includes/head', $settings);
          $this->load->view('admin/my_settings_view', $data);
          $this->load->view('admin/includes/footer');
     }

     public function settings_validation(){     
          //LOADIN FORM VALIDATION LIBRARY
          $this->load->library('form_validation');
          //SETTING FORM VALIDATION RULES
          $this->form_validation->set_rules('firstname', 'Firstame', 'required|trim|xss_clean');
          $this->form_validation->set_rules('lastname', 'Lastname', 'required|trim|xss_clean');

          //CHECKING IF OLD EMAIL MACHTING NEW EMAIL
          if($this->input->post('email') == $this->input->post('omail')){
               //TRUE DO NOT LOOK FOR UNIQUE
               $this->form_validation->set_rules('email', 'Email', 'valid_email|required|trim|xss_clean');
          }else{
               //FALSE LOOK IF NEW EMAIL IS IN DB ALREADY
               $this->form_validation->set_rules('email', 'Email', 'valid_email|required|trim|xss_clean|is_unique[users.email]');
          }

          $this->form_validation->set_rules('language', 'Language', 'required|trim|xss_clean');

          //SETTING VALIDATION ERRORS MESSAGE
          $this->form_validation->set_message('validate_adduser_credentials', '%s is already in use.');
          $this->form_validation->set_message('is_unique', '%s is not available');
          $this->form_validation->set_message('required', '<li>%s is required</li>');
          $this->form_validation->set_message('alpha_numeric', '%s can only contain [a-Z] and [0-9].');
          $this->form_validation->set_message('alpha', '%s can only contain letters.');
          $this->form_validation->set_message('valid_email', '%s address is not valid.');

          //IF VALIDATION SUCCEEDED RUN FUNCTION
          if($this->form_validation->run()){
               $this->load->model("users_model");
               $this->users_model->update_mysettings();

               //CREATING SUCCES MESSAGE
               $this->session->set_flashdata('success', 'The user has been updated.');

               //REDIRECT TO SAME PAGE WITH SUCCES MESSAGE
               redirect('users/mysettings');
          }else{
               //IF VALIDATION FAILS RETURN ERRORS AND SET SESSION DATA FOR LAST INPUTS
               $this->session->set_flashdata('firstname', $this->input->post('firstname'));
               $this->session->set_flashdata('lastname', $this->input->post('lastname'));
               $this->session->set_flashdata('email', $this->input->post('email'));
               $this->session->set_flashdata('errors', validation_errors());
               //REDIRECT BACK TO PAGE AND SET ERROR MESSAGE
               redirect('users/mysettings');
          }
     }
}
?>