<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	// Front-page controller
class Page extends CI_Controller {
    public function __construct() {
        parent::__construct();  
        $this->load->model('settings_model');
        $this->load->model('modules_model'); // required for nav
        $language = $this->settings_model->get_language();
        $this->lang->load("nav", $language);
        $this->lang->load("page", $language); // load correct language file based on db setting. Default is english.
        $this->lang->load("dashboard", $language);
    }
    public function index(){
        if ($this->session->userdata('is_logged_in')){
            
            $settings['page_title'] = 'Pages &laquo; Simplicity';
            $settings['modules'] = $this->modules_model->get_modules(); // required for nav
            
			// Load models
            $this->load->model('pages_model');
            
            // Get data
            if($this->pages_model->get_pages()){
            $data['pages'] = $this->pages_model->get_pages();
            }
            else{
            $data['pages'] = '';
            }
            
            // Views
            $this->load->view('admin/includes/head', $settings);
            $this->load->view('admin/pages_view', $data);
            $this->load->view('admin/includes/footer');
		}
		else{
			redirect('admin/login');
		}
    }
    
    public function new_page(){
        if ($this->session->userdata('is_logged_in')){
            
            $settings['page_title'] = 'Add New Page &laquo; Simplicity'; 
            $settings['modules'] = $this->modules_model->get_modules(); // required for nav
            
            $this->load->view('admin/includes/head', $settings);
            $this->load->view('admin/new_page_view');
            $this->load->view('admin/includes/footer');
        }
    }
    
    	public function new_page_validation(){
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');

		
		$this->form_validation->set_message('is_unique', '%s is not available');
		$this->form_validation->set_message('required', '<li>%s is required</li>');
		$this->form_validation->set_message('alpha_numeric', '%s can only contain [a-Z] and [0-9].');
		
		if ($this->form_validation->run()){
		
			$this->load->model('pages_model');
			
			if ($this->pages_model->new_page()){
				$this->session->set_flashdata('success', 'Page ' . $this->input->post('title') . ' has been created.'); //Success message if page is created.
				redirect('admin/pages');
			}
			else{
				$this->session->set_flashdata('error', 'Unable to create page: ' . $this->input->post('title') .'.');
			}
		}
		else{
				$this->session->set_flashdata('title', $this->input->post('title'));
				$this->session->set_flashdata('body', $this->input->post('body'));
				$this->session->set_flashdata('errors', validation_errors());
				redirect('admin/new-page');
			}
	}
    
    function save_page_order(){    
        $this->load->model('pages_model');
        
        if($this->pages_model->save_page_order()){
             echo json_encode(array('status' => TRUE)); // Use this to display a success message to the end user!
        }
        else{
            echo json_encode(array('status' => FALSE)); // Use this to display a success message to the end user!
        }
 

    }
    
    public function edit_page(){
        $settings['page_title'] = 'Edit Page &laquo; Simplicity'; 
        $settings['modules'] = $this->modules_model->get_modules(); // required for nav
		if ($this->session->userdata('is_logged_in')){
			$this->load->model('pages_model');
			
			if($data['pages'] = $this->pages_model->retrieve_page()){
			
                $this->load->view('admin/includes/head', $settings);
                $this->load->view('admin/edit_page_view', $data);
                $this->load->view('admin/includes/footer');
            }
            else{
                $this->session->set_flashdata('errors', lang('page_error_notfound'));
                redirect('admin/pages');
            }
		}
	}
	
	public function update_page(){
	   $this->load->model('pages_model');	
	
	   $data['pages'] = $this->pages_model->get_pages();
		  if ($this->pages_model->update_page()){
			$this->session->set_flashdata('success', 'Page ' . $this->input->post('title') . ' has been updated.');
			redirect('admin/pages');
		}
		else{
				$this->session->set_flashdata('errors', 'Unable to update ' . $this->input->post('title') . '.');
				redirect('admin/pages');
			}
	}
    
    public function delete_page(){
        $this->load->model('pages_model');
       
        if ($this->pages_model->delete_page()){

            $this->session->set_flashdata('success', 'Page has succesfully been removed');
			redirect('admin/pages');
        }
        else{
            $this->session->set_flashdata('errors', 'An error was encountered trying to delete the page');
			redirect('admin/pages');
        }
	}
    
} // End of class