<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	// Front-page controller
class Home extends CI_Controller {
    public function __construct() {
        parent::__construct();  
        
        // check if install directory exists
        $dir = getcwd();
        if(is_dir($dir.'/install/')){
           header('Location: install/');
        }
        else{
	}
}

	public function index(){
        $this->load->model('settings_model');
        $data['site_title'] = $this->settings_model->get_site_title();
        
		$this->load->model('pages_model');
		//get segment from url
		$getPage = $this->uri->segment(1);
        $data['pages'] = $this->pages_model->get_pages();
		
        if($getPage){
			$data['page'] = $this->pages_model->get_page($getPage);
						
			if(!$data['page']){
                show_404(); // show 404 page not found
			}
			else{
                $this->load->view('index_view', $data);
			}
		}
		else{
			if($data['page'] = $this->pages_model->first_page()){
            $this->load->view('index_view', $data);
            }
            else{
                echo "This website has no content";
            }
		}
		
	}
}

