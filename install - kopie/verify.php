<?php
session_start();
    $_SESSION['db_host'] = $_POST['db_host'];
    $_SESSION['db_name'] = $_POST['db_name'];
    $_SESSION['db_user'] = $_POST['db_user'];
    $_SESSION['db_password'] = $_POST['db_password'];

mysqli_report(MYSQLI_REPORT_STRICT);

try {
    $mysqli_connection = new MySQLi($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_name']);
    echo "true";
    // Execute sql

    $filename = 'simplicity.sql';

    $templine = '';
    // Read in entire file
    $lines = file($filename);
    // Loop through each line
    foreach ($lines as $line){
        // Skip it if it's a comment
    if (substr($line, 0, 2) == '--' || $line == '')
        continue;

        // Add this line to the current segment
        $templine .= $line;
        // If it has a semicolon at the end, it's the end of the query
        if (substr(trim($line), -1, 1) == ';'){
            // Perform the query
            mysqli_multi_query($mysqli_connection, $templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
        // Reset temp variable to empty
        $templine = '';
        }
    }    
} 
catch (Exception $e ) {
    echo "false";
    exit;
}
?>