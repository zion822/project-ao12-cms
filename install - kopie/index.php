<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Introduction</title>
    <link href="../assets/css/style.css" type="text/css" rel="stylesheet">
    <link href="../assets/css/bootstrap.css" type="text/css" rel="stylesheet">
    <script src="../assets/js/jquery-1.11.0.min.js"></script>

<script type='text/javascript' language='javascript'>

  $( document ).ready(function(){
      
$('#db_host').on('input', function() {
    var input=$(this);
    var hostname=input.val();
    if(hostname){input.removeClass("invalid").addClass("valid");}
    else{input.removeClass("valid").addClass("invalid");}
});
      
$('#db_name').on('input', function() {
    var input=$(this);
    var dbname=input.val();
    if(dbname){input.removeClass("invalid").addClass("valid");}
    else{input.removeClass("valid").addClass("invalid");}
});
      
$('#db_user').on('input', function() {
    var input=$(this);
    var dbuser=input.val();
    if(dbuser){input.removeClass("invalid").addClass("valid");}
    else{input.removeClass("valid").addClass("invalid");}
});
      
$("#db_submit button").click(function(event){
    $("#db_password").addClass("valid");
    var form_data=$("#db").serializeArray();
    var error_free=true;
    for (var input in form_data){
        var element=$("#db_"+form_data[input]['name']);
        var valid=element.hasClass("valid");
        var error_element=$("span", element.parent());
        if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}
        else{error_element.removeClass("error_show").addClass("error");}
    }
    if (!error_free){
        event.preventDefault();
    }
    else{
        //alert('No errors: Form will be submitted');
        validateConnection();
    }
});      
function validateConnection(){
    var db_host = $('#db_host').val();
    var db_name = $('#db_name').val();
    var db_user = $('#db_user').val();
    var db_password = $('#db_password').val();
    $.ajax({
        url: 'verify.php',
        data: ({db_host: db_host, db_name: db_name, db_user: db_user, db_password: db_password}),
        type: 'POST',
        success: function(data){
            if(data == "true"){
                window.location.href="?step=3";
            }
            else{
                $('#result').html("<div class='error_show'>Kan niet verbinden</div>");
            }
        }
       
    });
    
}
  });
</script>
</head>
<?php
    if(isset($_GET['step'])){
        $step = $_GET['step'];
        if($step == 2){
            $introActive = "";
            $dbActive = "class='install-active'";
            $admActive = "";
            $endActive = "";

            $header = "Database Instellingen";
            $subheader = "";
            $content = '<div class="install-container">
            <form role="form" id="db">
            <div id="result"></div>
  <div class="form-group">
    <label for="db_host">Hostnaam</label>
    <input type="text" class="form-control" id="db_host" name="host" placeholder="Hostnaam" required>
    <span class="error">Dit veld is verplicht.</span>
  </div>
  <div class="form-group">
    <label for="db_name">Database naam</label>
    <input type="text" class="form-control" id="db_name" name="name" placeholder="Database naam" required>
    <span class="error">Dit veld is verplicht.</span>
  </div>  
  <div class="form-group">
    <label for="db_user">Database gebruiker</label>
    <input type="text" class="form-control" id="db_user" name="user" placeholder="Database gebruiker" required>
    <span class="error">Dit veld is verplicht.</span>
  </div>  
  <div class="form-group">
    <label for="db_password">Database wachtwoord</label>
    <input type="password" class="form-control" id="db_password" name="password" placeholder="Database wachtwoord">
    
  </div>
  
</form>
<div id="db_submit">
    <button class="btn btn-default">Volgende stap</button></div>
</div>';
            }
        
        elseif($step == 3){
            if(isset($_GET['error'])){
                $pwnomatch = '<div class="error_show">Ingevoerde wachtwoorden komen niet overeen.</div>';
            }else{$pwnomatch = '';}
            $introActive = "";
            $dbActive = "";
            $admActive = "class='install-active'";
            $endActive = "";
            
            $header = "Beheerders details";
            $subheader = "";
            $content = '<div class="install-container">
            '.$pwnomatch.'
            <form role="form" id="user" method="post" action="?step=4">
    <div class="form-group">
        <label for="user_username">E-mail</label>
        <input type="text" class="form-control" id="user_username" name="username" placeholder="E-mail" required>
        <span class="error">Dit veld is verplicht.</span>
    </div>
    <div class="form-group">
        <label for="user_firstname">Naam</label>
        <input type="text" class="form-control" id="user_firstname" name="firstname" placeholder="Naam" required>
        <span class="error">Dit veld is verplicht.</span>
    </div>
    <div class="form-group">
        <label for="user_lastname">Achternaam</label>
        <input type="text" class="form-control" id="user_lastname" name="lastname" placeholder="Achternaam" required>
        <span class="error">Dit veld is verplicht.</span>
    </div>
    
    <div class="form-group">
        <label for="user_password">Wachtwoord</label>
        <input type="password" class="form-control" id="user_password" name="password" placeholder="Wachtwoord" required>
        <span class="error">Dit veld is verplicht.</span>
    </div>
    <div class="form-group">
        <label for="user_password2">Herhaal Wachtwoord</label>
        <input type="password" class="form-control" id="user_password2" name="password2" placeholder="Herhaal wachtwoord" required>
        <span class="error">Dit veld is verplicht.</span>
    </div>    
    <div class="form-group">
        <label for="user_language">Taal</label>
        <select class="form-control" id="user_language" name="language">
            <option value="dutch">Nederlands</option>
            <option value="english">English</option>
        </select>
        <span class="error">Dit veld is verplicht.</span>
    </div>  
    
    <div id="user_submit">
        <button class="btn btn-default">Volgende stap</button></div>
        </div>
</form>
';
        }
        elseif($step == 4){
            $introActive = "";
            $dbActive = "";
            $admActive = "";
            $endActive = "class='install-active'";
            
            $password = $_POST['password'];
            $password2 = $_POST['password2'];
            
            $header = 'Einde';
            $subheader = '';
            $content = '<p>Gefeliciteerd! De installatie is geslaagd.</p>
                    <p>Klik <a href="../admin">hier</a> om door te gaan naar het beheerderspaneel.</p>';
            
            if($password == $password2){
                $password = sha1($password);
                $username = $_POST['username'];
                $language = $_POST['language'];
                $firstname = $_POST['firstname'];
                $lastname = $_POST['lastname'];
                
                // Voeg eerste user toe.
                $mysqli_connection = new MySQLi($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_name']);
                mysqli_query($mysqli_connection, "INSERT INTO users(`role`, `password`, `email`, `firstname`, `lastname`, `language`, `email_code`, `active`) VALUES (1, '$password', '$username', '$firstname', '$lastname', '$language', 0, 1)");
                
                mysqli_query($mysqli_connection, "UPDATE settings SET setting_value = '$username' WHERE setting_name = 'admin_email'");
                
                // Wijzig DB gegevens in database.php
                $file = '../application/config/database.php';
                $file_contents = file_get_contents($file);
                $file_contents = str_replace('db_host', $_SESSION['db_host'], $file_contents);
                file_put_contents($file, $file_contents);
                
                $file_contents = file_get_contents($file);
                $file_contents = str_replace('db_name', $_SESSION['db_name'], $file_contents);
                file_put_contents($file, $file_contents);
                
                $file_contents = file_get_contents($file);
                $file_contents = str_replace('db_user', $_SESSION['db_user'], $file_contents);
                file_put_contents($file, $file_contents);
                
                $file_contents = file_get_contents($file);
                $file_contents = str_replace('db_password', $_SESSION['db_password'], $file_contents);
                file_put_contents($file, $file_contents);
                
                // check if install directory exists
                $dir = getcwd();
                if(is_dir($dir.'/install/')){
                    unlink($dir.'/install/index.php');
                    unlink($dir.'/install/install.php');
                    unlink($dir.'/install/verify.php');
                    unlink($dir.'/install/simplicity.sql');
                    rmdir($dir.'/install/');
                }
                else{}
            }

            else{
                header("Location: ?step=3&error=1");
                die();
            }
        }
        else{
            echo '<script>window.history.back();</script>';
        }
    }
    
    else{
            $introActive = "class='install-active'";
            $dbActive = "";
            $admActive = "";
            $endActive = "";
        
        $header = "Introductie";
        $subheader = "Welkom bij Simplicity";
        $content = '<p>Om verder te kunnen heb je de database instellingen nodig. Zonder deze gegevens kun je niet verder. De volgende gegevens heb je nodig: </p>
                <ul>
                    <li>Hostnaam - Het adres van de databaseserver.</li>
                    <li>Database naam - De naam van de database.</li>
                    <li>Database gebruikersnaam - De gebruikersnaam van de database.</li>
                    <li>Database wachtwoord - Het wachtwoord bij de gebruikersnaam.</li>
                </ul>
                <button type="submit" class="btn btn-default" onclick="window.location.href=\'?step=2\'";>Volgende stap</button>
                ';
    }
?>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="install-menu">
                    <ul class="install-nav">
                        <li <?php echo $introActive; ?>>Introductie</li>
                        <li <?php echo $dbActive; ?>>Database instellingen</li>
                        <li <?php echo $admActive; ?>>Beheerders details</li>
                        <li <?php echo $endActive;?>>Einde</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <h1><?php echo $header; ?></h1>
                <h3><?php echo $subheader; ?></h3>
                <?php echo $content;?>
            </div>
        </div>
    </div>
</body>
</html>