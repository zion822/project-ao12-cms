-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generatie Tijd: 05 Jun 2014 om 14:00
-- Server versie: 5.1.71
-- PHP Versie: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `JosNick`
--

-- --------------------------------------------------------

--
-- Tabel structuur voor tabel `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `mod_name` varchar(64) NOT NULL,
  `mod_author` varchar(64) NOT NULL,
  `mod_description` text NOT NULL,
  `mod_active` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Gegevens worden uitgevoerd voor tabel `modules`
--

INSERT INTO `modules` (`mid`, `mod_name`, `mod_author`, `mod_description`, `mod_active`) VALUES
(1, 'CSS Editor', 'Simplicity Dev Team', 'Simple CSS Editor', 1);
