-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2014 at 07:45 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simplicity`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `role` int(11) NOT NULL DEFAULT '3',
  `u_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User id',
  `password` varchar(255) NOT NULL COMMENT 'User password',
  `email` varchar(255) NOT NULL COMMENT 'User email',
  `firstname` varchar(255) NOT NULL COMMENT 'Firstname',
  `lastname` varchar(255) NOT NULL COMMENT 'Lastname',
  `language` varchar(20) NOT NULL DEFAULT 'English',
  `email_code` varchar(64) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0' COMMENT 'user active/first login',
  `recover_code` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='User tabel' AUTO_INCREMENT=22 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`role`, `u_id`, `password`, `email`, `firstname`, `lastname`, `language`, `email_code`, `active`, `recover_code`) VALUES
(1, 13, '8cb2237d0679ca88db6464eac60da96345513964', 'frolichit@gmail.com', 'Jos', 'Frolich', 'dutch', '975d1dacd5aedd432078823498f0793ef88ba46c', 0, '5e4316d55bb1ae56859139b50c51be8379362bd4'),
(2, 17, '8cb2237d0679ca88db6464eac60da96345513964', 'anton@prins.nl', 'Anton', 'Prins', 'english', '1eeaec5aea307ad0c13532e6733625083cf07551', 0, '0'),
(1, 10, '8cb2237d0679ca88db6464eac60da96345513964', 'Nick@grijpma.nl', 'Nick', 'Grijpma', 'english', 'eb22d9adfd86cdaa9840a3b28216dd9bb4941702', 0, '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
