-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Gegenereerd op: 13 jun 2014 om 12:26
-- Serverversie: 5.5.36
-- PHP-versie: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `simplicity`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `l_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(1) NOT NULL,
  `description` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `datetime` varchar(18) NOT NULL,
  PRIMARY KEY (`l_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=96 ;

--
-- Gegevens worden geëxporteerd voor tabel `logs`
--

INSERT INTO `logs` (`l_id`, `type`, `description`, `username`, `datetime`) VALUES
(10, 1, 'Successful login', 'jos', '15-04-2014, 11:58'),
(11, 0, 'Failed login', 'daowdi', '15-04-2014, 12:01'),
(12, 1, 'Successful login', 'jos', '15-04-2014, 12:01'),
(13, 0, 'Failed login', 'jos', '15-04-2014, 12:04'),
(14, 1, 'Successful login', 'jos', '15-04-2014, 12:04'),
(15, 1, 'Successful login', 'jos', '22-04-2014, 14:22'),
(16, 1, 'Successful login', 'jos', '01-05-2014, 11:10'),
(17, 1, 'Successful login', 'jos', '07-05-2014, 11:10'),
(18, 1, 'Successful login', 'jos', '07-05-2014, 11:18'),
(19, 1, 'Successful login', 'jos', '13-05-2014, 13:44'),
(20, 1, 'Successful login', 'jos', '19-05-2014, 14:08'),
(21, 1, 'Successful login', 'Jos', '19-05-2014, 14:08'),
(22, 1, 'Successful login', 'jos', '19-05-2014, 14:22'),
(23, 1, 'Successful login', 'jos', '20-05-2014, 13:27'),
(24, 0, 'Failed login', 'jos', '20-05-2014, 13:54'),
(25, 0, 'Failed login', 'jos', '20-05-2014, 13:57'),
(26, 0, 'Failed login', 'jos', '20-05-2014, 13:57'),
(27, 0, 'Failed login', 'jos', '20-05-2014, 13:57'),
(28, 0, 'Failed login', 'jos', '20-05-2014, 14:02'),
(29, 1, 'Successful login', 'jos', '20-05-2014, 14:03'),
(30, 1, 'Successful login', 'jos', '20-05-2014, 14:03'),
(31, 1, 'Successful login', 'jos', '20-05-2014, 14:26'),
(32, 1, 'Successful login', 'jos', '20-05-2014, 15:16'),
(33, 0, 'Failed login', 'jos', '28-05-2014, 09:06'),
(34, 1, 'Successful login', 'jos', '28-05-2014, 09:13'),
(35, 0, 'Failed login', 'josfrolich', '28-05-2014, 09:14'),
(36, 1, 'Successful login', 'jos', '28-05-2014, 09:14'),
(37, 0, 'Failed login', 'jos', '28-05-2014, 09:19'),
(38, 0, 'Failed login', 'frolichit@gmail.com', '28-05-2014, 09:19'),
(39, 1, 'Successful login', 'jos', '28-05-2014, 09:19'),
(40, 0, 'Failed login', 'jos', '28-05-2014, 09:27'),
(41, 1, 'Successful login', 'jos', '28-05-2014, 09:27'),
(42, 1, 'Successful login', 'jos', '02-06-2014, 13:39'),
(43, 1, 'Successful login', 'jos', '02-06-2014, 13:39'),
(44, 1, 'Successful login', 'jos', '02-06-2014, 13:40'),
(45, 1, 'Successful login', 'jos', '03-06-2014, 08:46'),
(46, 1, 'Successful login', 'jos', '03-06-2014, 08:53'),
(47, 1, 'Successful login', 'jos', '03-06-2014, 12:21'),
(48, 1, 'Successful login', 'jos', '03-06-2014, 12:23'),
(49, 1, 'Successful login', 'jos', '03-06-2014, 18:12'),
(50, 0, 'Failed login', 'jos', '03-06-2014, 18:13'),
(51, 0, 'Failed login', 'jos', '03-06-2014, 18:13'),
(52, 1, 'Successful login', 'jos', '03-06-2014, 18:13'),
(53, 0, 'Failed login', 'jos', '04-06-2014, 08:36'),
(54, 1, 'Successful login', 'jos', '04-06-2014, 08:36'),
(55, 1, 'Successful login', 'nickgrijpma', '04-06-2014, 08:38'),
(56, 0, 'Failed login', 'jos', '04-06-2014, 09:16'),
(57, 1, 'Successful login', 'jos', '04-06-2014, 09:16'),
(58, 1, 'Successful login', 'nickgrijpma', '04-06-2014, 09:16'),
(59, 1, 'Successful login', 'jos', '04-06-2014, 09:16'),
(60, 1, 'Successful login', 'nickgrijpma', '04-06-2014, 09:16'),
(61, 1, 'Successful login', 'nickgrijpma', '04-06-2014, 09:17'),
(62, 0, 'Failed login', 'jos', '04-06-2014, 09:53'),
(63, 1, 'Successful login', 'jos', '04-06-2014, 09:53'),
(64, 1, 'Successful login', 'nickgrijpma', '04-06-2014, 10:35'),
(65, 0, 'Failed login', 'jos', '04-06-2014, 10:36'),
(66, 0, 'Failed login', 'jos', '04-06-2014, 10:36'),
(67, 0, 'Failed login', 'jos', '04-06-2014, 10:36'),
(68, 1, 'Successful login', 'jos', '04-06-2014, 10:36'),
(69, 1, 'Successful login', 'jos', '05-06-2014, 22:32'),
(70, 1, 'Successful login', 'jos', '10-06-2014, 08:37'),
(71, 1, 'Successful login', 'jos', '10-06-2014, 09:02'),
(72, 1, 'Successful login', 'jos', '10-06-2014, 09:02'),
(73, 0, 'Failed login', 'test', '10-06-2014, 09:02'),
(74, 0, 'Failed login', 'Test', '10-06-2014, 09:02'),
(75, 1, 'Successful login', 'testtest', '10-06-2014, 09:03'),
(76, 1, 'Successful login', 'testtest', '10-06-2014, 09:04'),
(77, 1, 'Successful login', 'testtest', '10-06-2014, 09:11'),
(78, 1, 'Successful login', 'testtest', '10-06-2014, 09:15'),
(79, 0, 'Failed login', 'jos', '10-06-2014, 09:18'),
(80, 1, 'Successful login', 'jos', '10-06-2014, 09:18'),
(81, 0, 'Failed login', 'josfrolich', '11-06-2014, 08:39'),
(82, 0, 'Failed login', 'josfrolich', '11-06-2014, 08:39'),
(83, 0, 'Failed login', 'josfrolich', '11-06-2014, 08:39'),
(84, 0, 'Failed login', 'josfrolich', '11-06-2014, 08:39'),
(85, 1, 'Successful login', 'jos', '11-06-2014, 08:39'),
(86, 1, 'Successful login', 'jos', '11-06-2014, 08:41'),
(87, 1, 'Successful login', 'jos', '11-06-2014, 08:50'),
(88, 1, 'Successful login', 'jos', '11-06-2014, 08:51'),
(89, 1, 'Successful login', 'jos', '11-06-2014, 08:53'),
(90, 1, 'Successful login', 'jos', '11-06-2014, 09:04'),
(91, 1, 'Successful login', 'jos', '11-06-2014, 09:05'),
(92, 1, 'Successful login', 'jos', '11-06-2014, 09:28'),
(93, 1, 'Successful login', 'frolichit@gmail.com', '13-06-2014, 08:34'),
(94, 0, 'Failed login', 'frolichit@gmail.com', '13-06-2014, 09:25'),
(95, 1, 'Successful login', 'frolichit@gmail.com', '13-06-2014, 09:25');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `mod_name` varchar(64) NOT NULL,
  `mod_author` varchar(64) NOT NULL,
  `mod_description` text NOT NULL,
  `mod_active` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Gegevens worden geëxporteerd voor tabel `modules`
--

INSERT INTO `modules` (`mid`, `mod_name`, `mod_author`, `mod_description`, `mod_active`) VALUES
(1, 'CSS Editor', 'Simplicity Dev Team', 'Simple CSS Editor', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `order` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`p_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Gegevens worden geëxporteerd voor tabel `pages`
--

INSERT INTO `pages` (`p_id`, `page_title`, `content`, `order`) VALUES
(7, 'Nick', '<p>Arie!!!!!</p>', 99);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(60) NOT NULL,
  `setting_value` longtext NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Gegevens worden geëxporteerd voor tabel `settings`
--

INSERT INTO `settings` (`setting_id`, `setting_name`, `setting_value`) VALUES
(1, 'site_title', 'test title'),
(2, 'site_slogan', 'test slogan'),
(3, 'admin_email', 'admin@simplicity.nl');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `role` int(11) NOT NULL DEFAULT '3',
  `u_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User id',
  `password` varchar(255) NOT NULL COMMENT 'User password',
  `email` varchar(255) NOT NULL COMMENT 'User email',
  `firstname` varchar(255) NOT NULL COMMENT 'Firstname',
  `lastname` varchar(255) NOT NULL COMMENT 'Lastname',
  `language` varchar(20) NOT NULL DEFAULT 'English',
  `email_code` varchar(64) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0' COMMENT 'user active/first login',
  `recover_code` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='User tabel' AUTO_INCREMENT=22 ;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`role`, `u_id`, `password`, `email`, `firstname`, `lastname`, `language`, `email_code`, `active`, `recover_code`) VALUES
(1, 13, '356a192b7913b04c54574d18c28d46e6395428ab', 'frolichit@gmail.com', 'Jos', 'Frolich', 'dutch', '975d1dacd5aedd432078823498f0793ef88ba46c', 0, '5e4316d55bb1ae56859139b50c51be8379362bd4'),
(2, 17, '8cb2237d0679ca88db6464eac60da96345513964', 'anton@prins.nl', 'Anton', 'Prins', 'english', '1eeaec5aea307ad0c13532e6733625083cf07551', 0, '0'),
(1, 10, '8cb2237d0679ca88db6464eac60da96345513964', 'Nick@grijpma.nl', 'Nick', 'Grijpma', 'english', 'eb22d9adfd86cdaa9840a3b28216dd9bb4941702', 0, '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
