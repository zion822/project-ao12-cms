<?php
if(isset($_POST['content'])){
    if(file_put_contents('../../../assets/frontend.css', $_POST['content'])){
        echo "true";
    }
    else{
       echo "false";
    }
    exit();
}
else{ }// do nothing
$css = file_get_contents('assets/frontend.css');

?>
<script src="<?php echo base_url('assets/js/codemirror-4.2/lib/codemirror.js');?>"></script>
<?php echo link_tag('assets/js/codemirror-4.2/lib/codemirror.css');?>
<script src="<?php echo base_url('assets/js/codemirror-4.2/mode/css/css.js');?>"></script>
<script src="<?php echo base_url('assets/js/codemirror-4.2/addon/edit/closebrackets.js');?>"></script>
<div id="result">
    
</div>
<textarea id='CSS_editor'><?php echo $css;?></textarea>
<script>
    var editor = CodeMirror.fromTextArea(document.getElementById("CSS_editor"), {
    lineNumbers: true,
    matchBrackets: true,
    autoCloseBrackets: true,
    mode: "css"
  });

    

function saveFile(){
    var result = editor.getValue();
    
    $.ajax({  
    type: 'POST',  
    url: '../../assets/modules/CSS Editor/CSS Editor.php', 
    data: { content: result },
    success: function(data) {
        if(data == 'true'){
        $('#result').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check"></i> <strong>Success</strong> File saved! </div>');
        }
        else{
        $('#result').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times"></i> <strong>Error!</strong> Unable to save file. </div>');    
        }
    }
});
}
</script>
<br>
<button onclick="saveFile()" class='btn btn-success'>Save file</button>